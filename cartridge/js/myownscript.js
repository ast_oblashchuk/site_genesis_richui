//***** for change image in product tile
$( "body" ).on('click', '.previmageintile', function () {
	changeImageInTile('minus', this);
	//console.log('sasgfdg');
});

$( "body" ).on('click', '.nextimageintile', function () {
	changeImageInTile('plus', this);
});

$('div.product-tile button.previmageintile').attr('disabled', true);
var myGlobalVar = 0;
var globalProduct = null;
function changeImageInTile(direction, currentButton){

	var productId = $(currentButton).parent().parent("div.product-tile").attr("data-itemid");
	var arrayOfURLs = $(".divforimageurl");
	var urlObject = {};
	arrayOfURLs.each(function(){
		var oneUrl = $(this).attr('data-forimagesarr');
		var currentProductId = $(this).parent().parent("div.product-tile").attr("data-itemid");
		if(urlObject[currentProductId] == null){
			urlObject[currentProductId] = [];
		}
		urlObject[currentProductId].push(oneUrl);
    });
	
	if(globalProduct != productId){
		var currentImage = $('div.product-tile[data-itemid='+productId+']').find('a.thumb-link img').attr('src');
		for(var i=0; i <= urlObject[productId].length; i++){
			if(urlObject[productId][i] == currentImage){
				myGlobalVar = i;
			}
		}
	}
	
	if(direction == 'plus'){
		myGlobalVar++;
	}else{
		myGlobalVar--;
	}
	//var first = urlObject[productId][0];
	var last = urlObject[productId].length - 1;
	
	if(myGlobalVar <= 0){
		myGlobalVar = 0; 
		$('div.product-tile[data-itemid='+productId+'] button.previmageintile').attr('disabled', true);
	}else{
		$('div.product-tile[data-itemid='+productId+'] button.previmageintile').attr('disabled', false);
	}
	
	if(myGlobalVar >= last){
		myGlobalVar = last;
		$('div.product-tile[data-itemid='+productId+'] button.nextimageintile').attr('disabled', true);
	}else{
		$('div.product-tile[data-itemid='+productId+'] button.nextimageintile').attr('disabled', false);
	}
	
	var e = $("a.thumb-link[href]").each(function(){
		var currentProductId = $(this).parent().parent("div.product-tile").attr("data-itemid");
		if(productId == currentProductId){
			var newUrl = urlObject[productId][myGlobalVar];
			$(this).find('img').attr('src', newUrl);
		}
	});
	globalProduct = productId;	
}


$('.randomproductimage').on('click', function () {
	var pid = $(this).data('pid');
	$.ajax({
		url: Urls.testAjax,
		data: {pid: pid},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		$('div.resultproductobj').html('');
		$('div.resultproductobj').html('<p> name: '+data.name+'</p><p> price: '+data.price+'</p><p> desc: '+data.description+'</p>');
		$('.headimagefromrandomproducts').attr('src', data.image);
	});
});

$('.randommyorder').on('click', function () {
	var orderNo = $(this).data('oid');
	//console.log(orderNo);
	
	$.ajax({
		url: Urls.testAjax,
		data: {oid: orderNo},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		$('div.resultproductobj').html('');
		$('.headimagefromrandomproducts').attr('src', '');
		$('div.resultproductobj').html('<p> price: '+data.order+'</p>');
	});
	
});


/*var myownscript ={
	init: function(){
		console.log('inside');
	}
}
module.exports = myownscript;*/




// functional for add to my own list
var page = require('./page');

$( "body" ).on('click', '.product-actions .add-button-to-my-own-list', function () {
	
	var productId = $(this).data('product-id');
	
	$.ajax({
		url: Urls.isLogged,
		data: {pid: productId},
		dataType: 'json',
	}).done(function(data) {
		addItemInMyList(data.isAuthenticated);
	})
	
	function addItemInMyList(auth){
	
		if(auth){
			$.ajax({
				url: Urls.addToMyProductListAjax,
				data: {pid: productId},
				dataType: 'json',
			}).done(function(data) {
				//console.log(data);
				var message = "product '"+data.productName+"' was added to list";
				$(".product-actions .notify-after-added").html("<span style='color:#0000FF'>"+message+"</span>");
				
				setTimeout(function(){
					$(".product-actions .notify-after-added").html("");
				}, 2000);
				
				$(".product-actions a.add-button-to-my-own-list").html(Resources.REMOVE_FROM_LIST);
				$(".product-actions a.add-button-to-my-own-list").attr("class", "button simple remove-from-my-list");
				
			});
		}else{
			var url = require('./util').appendParamsToUrl(Urls.addToMyList, {redirecturl : window.location.href, pid: productId});
			//console.log(url);
			page.redirect(url);
		}
	}
	
});


$( "body" ).on('click', '.remove-from-my-own-list', function () {
	
	var productId = $(this).data('remove-from-my-own-list');
	var targetElement = $(this);
	
	$.ajax({
		url: Urls.removeFromMyProductListAjax,
		data: {pid: productId},
		dataType: 'json',
	}).done(function(data) {
		targetElement.closest("div.one-item-in-my-product-list").remove();
		var count = $("span.count-items-in-my-product-list").text() - 1;
		$("span.count-items-in-my-product-list").text(count)
	});
	
});


$( "body" ).on('click', '.remove-from-my-list', function () {
	
	var productId = $(this).data('product-id');
	var targetElement = $(this);
	
	$.ajax({
		url: Urls.removeFromMyProductListAjax,
		data: {pid: productId},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		var message = "product '"+data.productId+"' was deleted from list";
		$(".product-actions .notify-after-added").html("<span style='color:#0000FF'>"+message+"</span>");
		
		setTimeout(function(){
			$(".product-actions .notify-after-added").html("");
		}, 2000);
		
		$(".product-actions a.remove-from-my-list").html(Resources.ADD_IN_LIST);
		$(".product-actions a.remove-from-my-list").attr("class", "button simple add-button-to-my-own-list");
	});
	
});


//-------#########  for replenish

function getDefaultSelectedValue(){
	//var defaultV = $(".replenish-form").data('default-selected-value');
	return 3;
}

//$('.product-add-to-cart .field-wrapper .input-radio').change(function(){
//$('.product-add-to-cart').on('change', '.field-wrapper .input-radio', function(){
$('body').on('change', '.product-add-to-cart .field-wrapper input[id$="replenishform_autoreplenish"]', function(){
	
	//console.log("inside .product-add-to-cart");
	var selectedState = $(this).prop('value');
	var currentSelect = $(this).closest(".replenish-form").find('select');
	
	if(selectedState == 'false'){
		currentSelect.val(getDefaultSelectedValue());
		currentSelect.attr('disabled', 'disabled'); 
	}else{
		currentSelect.removeAttr('disabled'); 
	}
})

function change(pid, selectedState, frequencyState){
	$.ajax({
		url: Urls.changeReplenishStatus,
		data: {pid: pid, state:selectedState, frequency:frequencyState},
		dataType: 'json',
	}).done(function(data) {
		if(data.state){
			var selectorByAttr = "[data-pid-item="+pid+"]";
			$(selectorByAttr).find('select').removeAttr('disabled');
		}else{
			var selectorByAttr = "[data-pid-item="+pid+"]";
			$(selectorByAttr).find('select').val(getDefaultSelectedValue());
			$(selectorByAttr).find('select').attr('disabled', 'disabled');
		}
		$('.replenish-form .success-notify').html('successfully changed');
		setTimeout(function(){$('.replenish-form .success-notify').html('');}, 2000)
	});
}


$('.replenish-form-on-cart-page .field-wrapper .input-radio').change(function(){
//$(document).on('change', 'input[id$="replenishform_autoreplenish"]', function(){
	//console.log("inside .replenish-form-on-cart-page");
	var selectedFrequency = $(this).closest(".replenish-form-on-cart-page").find('select').val();
	var selectedState = $(this).prop('value');
	
	if(selectedState == 'false'){
		var frequencyState = getDefaultSelectedValue();
	}else{
		var frequencyState = selectedFrequency;
	}
	var pid = $(this).closest(".replenish-form-on-cart-page").data("pid-item");
	change(pid, selectedState, frequencyState);
});

$('.replenish-form-on-cart-page .field-wrapper select').change(function(){
	
	var pid = $(this).closest(".replenish-form-on-cart-page").data("pid-item");
	var selectedState = $(this).closest(".replenish-form-on-cart-page").find('.input-radio')[1].checked;
	if(selectedState){
		var frequencyState = $(this).val();
		change(pid, selectedState, frequencyState);
	}else{
		console.log('not select auto');
	}
});


$(".replenish-list-label").on("click", function(){
	$(this).next().toggle();
})



$( "body" ).on('click', '.remove-from-replenish-list', function () {
	
	var productId = $(this).data('remove-from-replenish-list');
	var targetElement = $(this);
	var listType = $(this).data('list-type');
	var replenishListId = $(this).data('replenish-list-id');
	
	$.ajax({
		url: Urls.removeFromReplenishList,
		data: {pid: productId, type: listType, lid: replenishListId},
		dataType: 'json',
	}).done(function(data) {
		console.log(data);
		
		var listDiv = targetElement.closest("div.one-replenish-list");
		var countEl = listDiv.find('span.count-in-list');
		countEl.each(function() {
			var count = $(this).text() - 1;
			$(this).text(count);
		});
		targetElement.closest("div.one-product-in-replenish-list").remove();
		
		var listDiv = targetElement.closest("div.one-replenish-list");
		var items = listDiv.find('div.one-product-in-replenish-list');
		if(data.length == 0){
			var selector = "div.one-replenish-list[data-lid='"+ data.lid +"']";
			$(selector).remove();
			//console.log('inside');
		}
	});
	
});


$( "body" ).on('click', '.remove-replenish-list', function () {
	
	var targetElement = $(this);
	var replenishListId = $(this).data('list-id');
	
	$.ajax({
		url: Urls.removeFromReplenishList,
		data: {lid: replenishListId},
		dataType: 'json',
	}).done(function(data) {
		console.log(data);
		targetElement.closest("div.one-replenish-list").remove();
	});
	
});
