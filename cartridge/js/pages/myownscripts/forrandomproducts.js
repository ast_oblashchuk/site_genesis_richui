$('.randomproductimage').on('click', function () {
	var pid = $(this).data('pid');
	$.ajax({
		url: Urls.testAjax,
		data: {pid: pid},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		$('div.resultproductobj').html('');
		$('div.resultproductobj').html('<p> name: '+data.name+'</p><p> price: '+data.price+'</p><p> desc: '+data.description+'</p>');
		$('.headimagefromrandomproducts').attr('src', data.image);
	});
});

$('.randommyorder').on('click', function () {
	var orderNo = $(this).data('oid');
	//console.log(orderNo);
	
	$.ajax({
		url: Urls.testAjax,
		data: {oid: orderNo},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		$('div.resultproductobj').html('');
		$('.headimagefromrandomproducts').attr('src', '');
		$('div.resultproductobj').html('<p> price: '+data.order+'</p>');
	});
	
});