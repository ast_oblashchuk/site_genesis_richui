//***** for change image in product tile
$( "body" ).on('click', '.previmageintile', function () {
	changeImageInTile('minus', this);
});

$( "body" ).on('click', '.nextimageintile', function () {
	changeImageInTile('plus', this);
});

$('div.product-tile button.previmageintile').attr('disabled', true);
var myGlobalVar = 0;
var globalProduct = null;
function changeImageInTile(direction, currentButton){

	var productId = $(currentButton).parent().parent("div.product-tile").attr("data-itemid");
	var arrayOfURLs = $(".divforimageurl");
	var urlObject = {};
	arrayOfURLs.each(function(){
		var oneUrl = $(this).attr('data-forimagesarr');
		var currentProductId = $(this).parent().parent("div.product-tile").attr("data-itemid");
		if(urlObject[currentProductId] == null){
			urlObject[currentProductId] = [];
		}
		urlObject[currentProductId].push(oneUrl);
    });
	
	if(globalProduct != productId){
		var currentImage = $('div.product-tile[data-itemid='+productId+']').find('a.thumb-link img').attr('src');
		for(var i=0; i <= urlObject[productId].length; i++){
			if(urlObject[productId][i] == currentImage){
				myGlobalVar = i;
			}
		}
	}
	
	if(direction == 'plus'){
		myGlobalVar++;
	}else{
		myGlobalVar--;
	}
	//var first = urlObject[productId][0];
	var last = urlObject[productId].length - 1;
	
	if(myGlobalVar <= 0){
		myGlobalVar = 0; 
		$('div.product-tile[data-itemid='+productId+'] button.previmageintile').attr('disabled', true);
	}else{
		$('div.product-tile[data-itemid='+productId+'] button.previmageintile').attr('disabled', false);
	}
	
	if(myGlobalVar >= last){
		myGlobalVar = last;
		$('div.product-tile[data-itemid='+productId+'] button.nextimageintile').attr('disabled', true);
	}else{
		$('div.product-tile[data-itemid='+productId+'] button.nextimageintile').attr('disabled', false);
	}
	
	var e = $("a.thumb-link[href]").each(function(){
		var currentProductId = $(this).parent().parent("div.product-tile").attr("data-itemid");
		if(productId == currentProductId){
			var newUrl = urlObject[productId][myGlobalVar];
			$(this).find('img').attr('src', newUrl);
		}
	});
	globalProduct = productId;	
}











$('.randomproductimage').on('click', function () {
	var pid = $(this).data('pid');
	$.ajax({
		url: Urls.testAjax,
		data: {pid: pid},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		$('div.resultproductobj').html('');
		$('div.resultproductobj').html('<p> name: '+data.name+'</p><p> price: '+data.price+'</p><p> desc: '+data.description+'</p>');
		$('.headimagefromrandomproducts').attr('src', data.image);
	});
});

$('.randommyorder').on('click', function () {
	var orderNo = $(this).data('oid');
	//console.log(orderNo);
	
	$.ajax({
		url: Urls.testAjax,
		data: {oid: orderNo},
		dataType: 'json',
	}).done(function(data) {
		//console.log(data);
		$('div.resultproductobj').html('');
		$('.headimagefromrandomproducts').attr('src', '');
		$('div.resultproductobj').html('<p> price: '+data.order+'</p>');
	});
	
});