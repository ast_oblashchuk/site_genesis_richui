'use strict';

module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt);
	//require('babel/register');

	// command line arguments
	var config = {};
	// mocha ui tests
	config.suite = grunt.option('suite') || '*';
	if (config.suite === 'all') { config.suite === '*'; }
	config.reporter = grunt.option('reporter') || 'spec';
	config.timeout = grunt.option('timeout') || 60000;
	config.port = grunt.option('port') || 7000;
	config.sourcemaps = !!grunt.option('sourcemaps');

	var paths = require('./package.json').paths;

	grunt.initConfig({
		
	
		watch: {
			scripts: {
				files: [paths.js.src + '**/*.js'],
				tasks: ['browserify:dev', 'sourcemap']
			},
			styles: {
				files: paths.css.map(function (path) {
					return path.src + '**/*.scss';
				}),
				tasks: ['css']
			},
			scriptsAndStyles: {
				files: [
					paths.js.src + '**/*.js', 
					paths.css.map(function (path) {
						return path.src + '**/*.scss';
					})
				],
				tasks: ['js', 'css']
			}
		},
		
		browserify: {
			dev: {
				files: [{
					src: paths.js.src + 'app.js',
					dest: paths.js.dest + 'app.js'
				}],
				options: {
					browserifyOptions: {
						debug: (config.sourcemaps)
					}
				}
			},
			watchDev: {
				files: [{
					src: paths.js.src + 'app.js',
					dest: paths.js.dest + 'app.js'
				}],
				options: {
					watch: true
				}
			},
			styleguide: {
				files: [{
					src: 'styleguide/js/main.js',
					dest: 'styleguide/dist/main.js'
				}],
				options: {
					transform: ['hbsfy']
				}
			},
			watchStyleguide: {
				files: [{
					src: 'styleguide/js/main.js',
					dest: 'styleguide/dist/main.js'
				}],
				options: {
					transform: ['hbsfy'],
					watch: true
				}
			}
		},
		external_sourcemap: {
			browserify: {
				files: [{
					dest: paths.js.dest,
					src: paths.js.dest + 'app.js'
				}]
			}
		},

		
		
		
		
		// for css
		sass: {
			dev: {
				options: {
					style: 'expanded',
					sourceMap: (config.sourcemaps)
				},
				files: paths.css.map(function (path) {
					return {src: path.src + 'style.scss', dest: path.dest + 'style.css'};
				})
			},
			styleguide: {
				files: [{
					'styleguide/dist/main.css': 'styleguide/scss/main.scss'
				}]
			}
		},
		autoprefixer: {
			dev: {
				files: paths.css.map(function (path) {
					return {src: path.dest + 'style.css', dest: path.dest + 'style.css'};
				})
			},
			styleguide: {
				files: [{
					'styleguide/dist/main.css': 'styleguide/dist/main.css'
				}]
			}
		},
		
		
		
		

		
	});

	grunt.registerTask('sourcemap', function () {
		if (config.sourcemaps) {
			grunt.task.run(['external_sourcemap:browserify']);
		}
	});
	
	grunt.registerTask('js', ['browserify:dev', 'sourcemap']);
	grunt.registerTask('css', ['sass:dev', 'autoprefixer:dev']);
	grunt.registerTask('scripts', ['js', 'watch:scripts']);
	grunt.registerTask('styles', ['css', 'watch:styles']);
	grunt.registerTask('default', ['js', 'css', 'watch:scriptsAndStyles']);
	
};