'use strict';

var progress = require('./progress'),
	util = require('./util');

var currentRequests = [];

/**
 * @function
 * @description Ajax request to get json response
 * @param {Boolean} async  Asynchronous or not
 * @param {String} url URI for the request
 * @param {Object} data Name/Value pair data request
 * @param {Function} callback  Callback function to be called
 */
var getJson = function (options) {
	options.url = util.toAbsoluteUrl(options.url);
	// return if no url exists or url matches a current request
	if (!options.url || currentRequests[options.url]) {
		return;
	}

	currentRequests[options.url] = true;

	// make the server call
	$.ajax({
		dataType: 'json',
		url: options.url,
		async: (typeof options.async === 'undefined' || options.async === null) ? true : options.async,
		data: options.data || {}
	})
	// success
	.done(function (response) {
		if (options.callback) {
			options.callback(response);
		}
	})
	// failed
	.fail(function (xhr, textStatus) {
		if (textStatus === 'parsererror') {
			window.alert(Resources.BAD_RESPONSE);
		}
		if (options.callback) {
			options.callback(null);
		}
	})
	// executed on success or fail
	.always(function () {
		// remove current request from hash
		if (currentRequests[options.url]) {
			delete currentRequests[options.url];
		}
	});
};
/**
 * @function
 * @description ajax request to load html response in a given container
 * @param {String} url URI for the request
 * @param {Object} data Name/Value pair data request
 * @param {Function} callback  Callback function to be called
 * @param {Object} target Selector or element that will receive content
 */
var load = function (options) {
	options.url = util.toAbsoluteUrl(options.url);
	// return if no url exists or url matches a current request
	if (!options.url || currentRequests[options.url]) {
		return;
	}

	currentRequests[options.url] = true;

	// make the server call
	$.ajax({
		dataType: 'html',
		url: util.appendParamToURL(options.url, 'format', 'ajax'),
		data: options.data,
		xhrFields: {
			withCredentials: true
		}
	})
	.done(function (response) {
		// success
		if (options.target) {
			$(options.target).empty().html(response);
		}
		if (options.callback) {
			options.callback(response);
		}
	})
	.fail(function (xhr, textStatus) {
		// failed
		if (textStatus === 'parsererror') {
			window.alert(Resources.BAD_RESPONSE);
		}
		options.callback(null, textStatus);
	})
	.always(function () {
		progress.hide();
		// remove current request from hash
		if (currentRequests[options.url]) {
			delete currentRequests[options.url];
		}
	});
};

exports.getJson = getJson;
exports.load = load;

/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require('./countries'),
	cq = require('./cq'),
	dialog = require('./dialog'),
	minicart = require('./minicart'),
	page = require('./page'),
	rating = require('./rating'),
	searchplaceholder = require('./searchplaceholder'),
	searchsuggest = require('./searchsuggest'),
	searchsuggestbeta = require('./searchsuggest-beta'),
	tooltip = require('./tooltip'),
	util = require('./util'),
	validator = require('./validator');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();

function initializeEvents() {
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

	$('body')
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length;

				if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
					e.preventDefault();
				}
		})
		.on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
		});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('#navigation .header-search');
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	// print handler
	$('.print-page').on('click', function () {
		window.print();
		return false;
	});

	// add show/hide navigation elements
	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});

	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function () {
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});

	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({color: '#333333'}, 500, 'linear');
		});
	}

	$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600
			}
		});
	});

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
	});
	$('.menu-category li .menu-item-toggle').on('click', function (e) {
		e.preventDefault();
		var $parentLi = $(e.target).closest('li');
		$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('fa-chevron-up active').addClass('fa-chevron-right');
		$parentLi.toggleClass('active');
		$(e.target).toggleClass('fa-chevron-right fa-chevron-up active');
	});
	$('.user-account').on('click', function (e) {
		e.preventDefault();
		$(this).parent('.user-info').toggleClass('active');
	});
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront'),
	wishlist: require('./pages/wishlist'),
	storelocator: require('./pages/storelocator')
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		initializeDom();
		initializeEvents();

		// init specific global components
		countries.init();
		tooltip.init();
		minicart.init();
		validator.init();
		rating.init();
		searchplaceholder.init();
		cq.init();
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

// initialize app
$(document).ready(function () {
	app.init();
});

'use strict';

var dialog = require('./dialog'),
	page = require('./page'),
	util = require('./util');

var selectedList = [];
var maxItems = 1;
var bliUUID = '';

/**
 * @private
 * @function
 * description Gets a list of bonus products related to a promoted product
 */
function getBonusProducts() {
	var o = {};
	o.bonusproducts = [];

	var i, len;
	for (i = 0, len = selectedList.length; i < len; i++) {
		var p = {
			pid: selectedList[i].pid,
			qty: selectedList[i].qty,
			options: {}
		};
		var a, alen, bp = selectedList[i];
		if (bp.options) {
			for (a = 0, alen = bp.options.length; a < alen; a++) {
				var opt = bp.options[a];
				p.options = {optionName:opt.name, optionValue:opt.value};
			}
		}
		o.bonusproducts.push({product:p});
	}
	return o;
}

var selectedItemTemplate = function (data) {
	var attributes = '';
	for (var attrID in data.attributes) {
		var attr = data.attributes[attrID];
		attributes += '<li data-attribute-id="' + attrID + '">\n';
		attributes += '<span class="display-name">' + attr.displayName + '</span>: ';
		attributes += '<span class="display-value">' + attr.displayValue + '</span>\n';
		attributes += '</li>';
	}
	attributes += '<li class="item-qty">\n';
	attributes += '<span class="display-name">Qty</span>: ';
	attributes += '<span class="display-value">' + data.qty + '</span>';
	return [
		'<li class="selected-bonus-item" data-uuid="' + data.uuid + '" data-pid="' + data.pid + '">',
		'<i class="remove-link fa fa-remove" title="Remove this product" href="#"></i>',
		'<div class="item-name">' + data.name + '</div>',
		'<ul class="item-attributes">',
		attributes,
		'<ul>',
		'<li>'
	].join('\n');
};

// hide swatches that are not selected or not part of a Product Variation Group
var hideSwatches = function () {
	$('.bonus-product-item .swatches li').not('.selected').not('.variation-group-value').hide();
	// prevent unselecting the selected variant
	$('.bonus-product-item .swatches .selected').on('click', function () {
		return false;
	});
};

/**
 * @private
 * @function
 * @description Updates the summary page with the selected bonus product
 */
function updateSummary() {
	var $bonusProductList = $('#bonus-product-list');
	if (selectedList.length === 0) {
		$bonusProductList.find('li.selected-bonus-item').remove();
	} else {
		var ulList = $bonusProductList.find('ul.selected-bonus-items').first();
		var i, len;
		for (i = 0, len = selectedList.length; i < len; i++) {
			var item = selectedList[i];
			var li = selectedItemTemplate(item);
			$(li).appendTo(ulList);
		}
	}

	// get remaining item count
	var remain = maxItems - selectedList.length;
	$bonusProductList.find('.bonus-items-available').text(remain);
	if (remain <= 0) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	} else {
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
	}
}

function initializeGrid () {
	var $bonusProduct = $('#bonus-product-dialog'),
		$bonusProductList = $('#bonus-product-list'),
	bliData = $bonusProductList.data('line-item-detail');
	maxItems = bliData.maxItems;
	bliUUID = bliData.uuid;

	if (bliData.itemCount >= maxItems) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	}

	var cartItems = $bonusProductList.find('.selected-bonus-item');
	cartItems.each(function () {
		var ci = $(this);
		var product = {
			uuid: ci.data('uuid'),
			pid: ci.data('pid'),
			qty: ci.find('.item-qty').text(),
			name: ci.find('.item-name').html(),
			attributes: {}
		};
		var attributes = ci.find('ul.item-attributes li');
		attributes.each(function () {
			var li = $(this);
			product.attributes[li.data('attributeId')] = {
				displayName:li.children('.display-name').html(),
				displayValue:li.children('.display-value').html()
			};
		});
		selectedList.push(product);
	});

	$bonusProductList.on('click', '.bonus-product-item a[href].swatchanchor', function (e) {
		e.preventDefault();
		var url = this.href,
			$this = $(this);
		url = util.appendParamsToUrl(url, {
			'source': 'bonus',
			'format': 'ajax'
		});
		$.ajax({
			url: url,
			success: function (response) {
				$this.closest('.bonus-product-item').empty().html(response);
				hideSwatches();
			}
		});
	})
	.on('change', '.input-text', function () {
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
		$(this).closest('.bonus-product-form').find('.quantity-error').text('');
	})
	.on('click', '.select-bonus-item', function (e) {
		e.preventDefault();
		if (selectedList.length >= maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			$bonusProductList.find('.bonus-items-available').text('0');
			return;
		}

		var form = $(this).closest('.bonus-product-form'),
			detail = $(this).closest('.product-detail'),
			uuid = form.find('input[name="productUUID"]').val(),
			qtyVal = form.find('input[name="Quantity"]').val(),
			qty = (isNaN(qtyVal)) ? 1 : (+qtyVal);

		if (qty > maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			form.find('.quantity-error').text(Resources.BONUS_PRODUCT_TOOMANY);
			return;
		}

		var product = {
			uuid: uuid,
			pid: form.find('input[name="pid"]').val(),
			qty: qty,
			name: detail.find('.product-name').text(),
			attributes: detail.find('.product-variations').data('attributes'),
			options: []
		};

		var optionSelects = form.find('.product-option');

		optionSelects.each(function () {
			product.options.push({
				name: this.name,
				value: $(this).val(),
				display: $(this).children(':selected').first().html()
			});
		});
		selectedList.push(product);
		updateSummary();
	})
	.on('click', '.remove-link', function (e) {
		e.preventDefault();
		var container = $(this).closest('.selected-bonus-item');
		if (!container.data('uuid')) { return; }

		var uuid = container.data('uuid');
		var i, len = selectedList.length;
		for (i = 0; i < len; i++) {
			if (selectedList[i].uuid === uuid) {
				selectedList.splice(i, 1);
				break;
			}
		}
		updateSummary();
	})
	.on('click', '.add-to-cart-bonus', function (e) {
		e.preventDefault();
		var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: bliUUID});
		var bonusProducts = getBonusProducts();
		if (bonusProducts.bonusproducts[0].product.qty > maxItems) {
			bonusProducts.bonusproducts[0].product.qty = maxItems;
		}
		// make the server call
		$.ajax({
			type: 'POST',
			dataType: 'json',
			cache: false,
			contentType: 'application/json',
			url: url,
			data: JSON.stringify(bonusProducts)
		})
		.done(function () {
			// success
			page.refresh();
		})
		.fail(function (xhr, textStatus) {
			// failed
			if (textStatus === 'parsererror') {
				window.alert(Resources.BAD_RESPONSE);
			} else {
				window.alert(Resources.SERVER_CONNECTION_ERROR);
			}
		})
		.always(function () {
			$bonusProduct.dialog('close');
		});
	});
}

var bonusProductsView = {
	/**
	 * @function
	 * @description Open the list of bonus products selection dialog
	 */
	show: function (url) {
		var $bonusProduct = $('#bonus-product-dialog');
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: url,
			options: {
				width: 795,
				title: Resources.BONUS_PRODUCTS
			},
			callback: function () {
				initializeGrid();
				hideSwatches();
			}
		});
	},
	/**
	 * @function
	 * @description Open bonus product promo prompt dialog
	 */
	loadBonusOption: function () {
		var	self = this,
			bonusDiscountContainer = document.querySelector('.bonus-discount-container');
		if (!bonusDiscountContainer) { return; }

		// get the html from minicart, then trash it
		var bonusDiscountContainerHtml = bonusDiscountContainer.outerHTML;
		bonusDiscountContainer.parentNode.removeChild(bonusDiscountContainer);

		dialog.open({
			html: bonusDiscountContainerHtml,
			options: {
				width: 400,
				title: Resources.BONUS_PRODUCT,
				buttons: [{
					text: Resources.SELECT_BONUS_PRODUCTS,
					click: function () {
						var uuid = $('.bonus-product-promo').data('lineitemid'),
							url = util.appendParamsToUrl(Urls.getBonusProducts, {
								bonusDiscountLineItemUUID: uuid,
								source: 'bonus'
							});
						$(this).dialog('close');
						self.show(url);
					}
				}, {
					text: Resources.NO_THANKS,
					click: function () {
						$(this).dialog('close');
					}
				}]
			},
			callback: function () {
				// show hide promo details
				$('.show-promo-details').on('click', function () {
					$('.promo-details').toggleClass('visible');
				});
			}
		});
	}
};

module.exports = bonusProductsView;

'use strict';

var page = require('./page'),
	util = require('./util'),
	TPromise = require('promise');

var _currentCategory = '',
	MAX_ACTIVE = 6;

/**
 * @private
 * @function
 * @description Verifies the number of elements in the compare container and updates it with sequential classes for ui targeting
 */
function refreshContainer() {
	var $compareContainer = $('.compare-items');
	var $compareItems = $compareContainer.find('.compare-item');
	var numActive = $compareItems.filter('.active').length;

	if (numActive < 2) {
		$('#compare-items-button').attr('disabled', 'disabled');
	} else {
		$('#compare-items-button').removeAttr('disabled');
	}

	$compareContainer.toggle(numActive > 0);
}
/**
 * @private
 * @function
 * @description Adds an item to the compare container and refreshes it
 */
function addToList(data) {
	// get the first compare-item not currently active
	var $item = $('.compare-items .compare-item').not('.active').first(),
		$productTile = $('#' + data.uuid);

	if ($item.length === 0) {
		if ($productTile.length > 0) {
			$productTile.find('.compare-check')[0].checked = false;
		}
		window.alert(Resources.COMPARE_ADD_FAIL);
		return;
	}

	// if already added somehow, return
	if ($('[data-uuid="' + data.uuid + '"]').length > 0) {
		return;
	}
	// set as active item
	$item.addClass('active')
		.attr('data-uuid', data.uuid)
		.attr('data-itemid', data.itemid)
		.data('uuid', data.uuid)
		.data('itemid', data.itemid)
		.append($(data.img).clone().addClass('compare-item-image'));
}
/**
 * @private
 * @function
 * description Removes an item from the compare container and refreshes it
 */
function removeFromList($item) {
	if ($item.length === 0) { return; }
	// remove class, data and id from item
	$item.removeClass('active')
		.removeAttr('data-uuid')
		.removeAttr('data-itemid')
		.data('uuid', '')
		.data('itemid', '')
		// remove the image
		.find('.compare-item-image').remove();
}

function addProductAjax(args) {
	var promise = new TPromise(function (resolve, reject) {
		$.ajax({
			url: Urls.compareAdd,
			data: {
				pid: args.itemid,
				category: _currentCategory
			},
			dataType: 'json'
		}).done(function (response) {
			if (!response || !response.success) {
				reject(new Error(Resources.COMPARE_ADD_FAIL));
			} else {
				resolve(response);
			}
		}).fail(function (jqxhr, status, err) {
			reject(new Error(err));
		});
	});
	return promise;
}

function removeProductAjax(args) {
	var promise = new TPromise(function (resolve, reject) {
		$.ajax({
			url: Urls.compareRemove,
			data: {
				pid: args.itemid,
				category: _currentCategory
			},
			dataType: 'json'
		}).done(function (response) {
			if (!response || !response.success) {
				reject(new Error(Resources.COMPARE_REMOVE_FAIL));
			} else {
				resolve(response);
			}
		}).fail(function (jqxhr, status, err) {
			reject(new Error(err));
		});
	});
	return promise;
}

function shiftImages() {
	return new TPromise(function (resolve) {
		var $items = $('.compare-items .compare-item');
		$items.each(function (i, item) {
			var $item = $(item);
			// last item
			if (i === $items.length - 1) {
				return removeFromList($item);
			}
			var $next = $items.eq(i + 1);
			if ($next.hasClass('active')) {
				// remove its own image
				$next.find('.compare-item-image').detach().appendTo($item);
				$item.addClass('active')
					.attr('data-uuid', $next.data('uuid'))
					.attr('data-itemid', $next.data('itemid'))
					.data('uuid', $next.data('uuid'))
					.data('itemid', $next.data('itemid'));
			}
		});
		resolve();
	});
}

/**
 * @function
 * @description Adds product to the compare table
 */
function addProduct(args) {
	var promise;
	var $items = $('.compare-items .compare-item');
	var $cb = $(args.cb);
	var numActive = $items.filter('.active').length;
	if (numActive === MAX_ACTIVE) {
		if (!window.confirm(Resources.COMPARE_CONFIRMATION)) {
			$cb[0].checked = false;
			return;
		}

		// remove product using id
		var $firstItem = $items.first();
		promise = removeItem($firstItem).then(function () {
			return shiftImages();
		});
	} else {
		promise = TPromise.resolve(0);
	}
	return promise.then(function () {
		return addProductAjax(args).then(function () {
			addToList(args);
			if ($cb && $cb.length > 0) { $cb[0].checked = true; }
			refreshContainer();
		});
	}).then(null, function () {
		if ($cb && $cb.length > 0) { $cb[0].checked = false; }
	});
}

/**
 * @function
 * @description Removes product from the compare table
 * @param {object} args - the arguments object should have the following properties: itemid, uuid and cb (checkbox)
 */
function removeProduct(args) {
	var $cb = args.cb ? $(args.cb) : null;
	return removeProductAjax(args).then(function () {
		var $item = $('[data-uuid="' + args.uuid + '"]');
		removeFromList($item);
		if ($cb && $cb.length > 0) { $cb[0].checked = false; }
		refreshContainer();
	}, function () {
		if ($cb && $cb.length > 0) { $cb[0].checked = true; }
	});
}

function removeItem($item) {
	var uuid = $item.data('uuid'),
		$productTile = $('#' + uuid);
	return removeProduct({
		itemid: $item.data('itemid'),
		uuid: uuid,
		cb: ($productTile.length === 0) ? null : $productTile.find('.compare-check')
	});
}

/**
 * @private
 * @function
 * @description Initializes the DOM-Object of the compare container
 */
function initializeDom() {
	var $compareContainer = $('.compare-items');
	_currentCategory = $compareContainer.data('category') || '';
	var $active = $compareContainer.find('.compare-item').filter('.active');
	$active.each(function () {
		var $productTile = $('#' +  $(this).data('uuid'));
		if ($productTile.length === 0) {return;}
		$productTile.find('.compare-check')[0].checked = true;
	});
	// set container state
	refreshContainer();
}

/**
 * @private
 * @function
 * @description Initializes the events on the compare container
 */
function initializeEvents() {
	// add event to buttons to remove products
	$('.compare-item').on('click', '.compare-item-remove', function () {
		removeItem($(this).closest('.compare-item'));
	});

	// Button to go to compare page
	$('#compare-items-button').on('click', function () {
		page.redirect(util.appendParamToURL(Urls.compareShow, 'category', _currentCategory));
	});

	// Button to clear all compared items
	// rely on refreshContainer to take care of hiding the container
	$('#clear-compared-items').on('click', function () {
		$('.compare-items .active').each(function () {
			removeItem($(this));
		});
	});
}

exports.init = function () {
	initializeDom();
	initializeEvents();
};

exports.addProduct = addProduct;
exports.removeProduct = removeProduct;

'use strict';

var dialog = require('./dialog');

/**
 * @function cookieprivacy	Used to display/control the scrim containing the cookie privacy code
 **/
module.exports = function () {
	/**
	 * If we have not accepted cookies AND we're not on the Privacy Policy page, then show the notification
	 * NOTE: You will probably want to adjust the Privacy Page test to match your site's specific privacy / cookie page
	 */
	if (SitePreferences.COOKIE_HINT === true && document.cookie.indexOf('dw_cookies_accepted') < 0) {
		// check for privacy policy page
		if ($('.privacy-policy').length === 0) {
			dialog.open({
				url: Urls.cookieHint,
				options: {
					closeOnEscape: false,
					dialogClass: 'no-close',
					buttons: [{
						text: Resources.I_AGREE,
						click: function () {
							$(this).dialog('close');
							enableCookies();
						}
					}]
				}
			});
		}
	} else {
		// Otherwise, we don't need to show the asset, just enable the cookies
		enableCookies();
	}

	function enableCookies() {
		if (document.cookie.indexOf('dw=1') < 0) {
			document.cookie = 'dw=1; path=/';
		}
		if (document.cookie.indexOf('dw_cookies_accepted') < 0) {
			document.cookie = 'dw_cookies_accepted=1; path=/';
		}
	}
};

'use strict';

exports.init = function init () {
	$('.country-selector .current-country').on('click', function () {
		$('.country-selector .selector').toggleClass('active');
		$(this).toggleClass('selector-active');
	});
	// set currency first before reload
	$('.country-selector .selector .locale').on('click', function (e) {
		e.preventDefault();
		var url = this.href;
		var currency = this.getAttribute('data-currency');
		$.ajax({
			dataType: 'json',
			url: Urls.setSessionCurrency,
			data: {
				format: 'ajax',
				currencyMnemonic: currency
			}
		})
		.done(function (response) {
			if (!response.success) {
				throw new Error('Unable to set currency');
			}
			window.location.href = url;
		});
	});
};

'use strict';
/* global CQuotient */

function clickThruAfter() {
	var recommenderName = localStorage.getItem('cq.recommenderName');
	var currentProductId = $('[itemprop="productID"]').data('masterid') || '';
	if (!recommenderName) {return;}
	var anchors;
	if (localStorage.getItem('cq.anchors')) {
		anchors = localStorage.getItem('cq.anchors');
		localStorage.removeItem('cq.anchors');
	}
	localStorage.removeItem('cq.recommenderName');
	if (window.CQuotient) {
		CQuotient.activities.push({
			activityType: 'clickReco',
			parameters: {
				cookieId: CQuotient.getCQCookieId(),
				userId: CQuotient.getCQUserId(),
				recommenderName: recommenderName,
				anchors: anchors || '',
				products: {
					id: currentProductId
				}
			}
		});
	}
}

exports.init = function () {
	// set cookie before click through from product tile
	$('body').on('click', '.product-tile[data-recommendername] a', function () {
		// if currently on a product page, send its productId as the anchor
		if (window.pageContext.type === 'product') {
			localStorage.setItem('cq.anchors', $('[itemprop="productID"]').data('masterid') || '');
		}
		var recommenderName = $(this).parents('.product-tile').data('recommendername');
		localStorage.setItem('cq.recommenderName', recommenderName);
	});

	clickThruAfter();
};

'use strict';

var ajax = require('./ajax'),
	util = require('./util'),
	_ = require('lodash'),
	imagesLoaded = require('imagesloaded');

var dialog = {
	/**
	 * @function
	 * @description Appends a dialog to a given container (target)
	 * @param {Object} params  params.target can be an id selector or an jquery object
	 */
	create: function (params) {
		var $target, id;

		if (_.isString(params.target)) {
			if (params.target.charAt(0) === '#') {
				$target = $(params.target);
			} else {
				$target = $('#' + params.target);
			}
		} else if (params.target instanceof jQuery) {
			$target = params.target;
		} else {
			$target = $('#dialog-container');
		}

		// if no element found, create one
		if ($target.length === 0) {
			if ($target.selector && $target.selector.charAt(0) === '#') {
				id = $target.selector.substr(1);
				$target = $('<div>').attr('id', id).addClass('dialog-content').appendTo('body');
			}
		}

		// create the dialog
		this.$container = $target;
		this.$container.dialog(_.merge({}, this.settings, params.options || {}));
	},
	/**
	 * @function
	 * @description Opens a dialog using the given url (params.url) or html (params.html)
	 * @param {Object} params
	 * @param {Object} params.url should contain the url
	 * @param {String} params.html contains the html of the dialog content
	 */
	open: function (params) {
		// close any open dialog
		this.close();
		this.create(params);
		this.replace(params);
	},
	/**
	 * @description populate the dialog with html content, then open it
	 **/
	openWithContent: function (params) {
		var content, position, callback;

		if (!this.$container) { return; }
		content = params.content || params.html;
		if (!content) { return; }
		this.$container.empty().html(content);
		if (!this.$container.dialog('isOpen')) {
			this.$container.dialog('open');
		}

		if (params.options) {
			position = params.options.position;
		}
		if (!position) {
			position = this.settings.position;
		}
		imagesLoaded(this.$container).on('done', function () {
			this.$container.dialog('option', 'position', position);
		}.bind(this));

		callback = (typeof params.callback === 'function') ? params.callback : function () {};
		callback();
	},
	/**
	 * @description Replace the content of current dialog
	 * @param {object} params
	 * @param {string} params.url - If the url property is provided, an ajax call is performed to get the content to replace
	 * @param {string} params.html - If no url property is provided, use html provided to replace
	 */
	replace: function (params) {
		if (!this.$container) {
			return;
		}
		if (params.url) {
			params.url = util.appendParamToURL(params.url, 'format', 'ajax');
			ajax.load({
				url: params.url,
				data: params.data,
				callback: function (response) {
					params.content = response;
					this.openWithContent(params);
				}.bind(this)
			});
		} else if (params.html) {
			this.openWithContent(params);
		}
	},
	/**
	 * @function
	 * @description Closes the dialog
	 */
	close: function () {
		if (!this.$container) {
			return;
		}
		this.$container.dialog('close');
	},
	/**
	 * @function
	 * @description Submits the dialog form with the given action
	 * @param {String} The action which will be triggered upon form submit
	 */
	submit: function (action) {
		var $form = this.$container.find('form:first');
		// set the action
		$('<input/>').attr({
			name: action,
			type: 'hidden'
		}).appendTo($form);
		// serialize the form and get the post url
		var data = $form.serialize();
		var url = $form.attr('action');
		// make sure the server knows this is an ajax request
		if (data.indexOf('ajax') === -1) {
			data += '&format=ajax';
		}
		// post the data and replace current content with response content
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			dataType: 'html',
			success: function (html) {
				this.$container.html(html);
			}.bind(this),
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
			}
		});
	},
	exists: function () {
		return this.$container && (this.$container.length > 0);
	},
	isActive: function () {
		return this.exists() && (this.$container.children.length > 0);
	},
	settings: {
		autoOpen: false,
		height: 'auto',
		modal: true,
		overlay: {
			opacity: 0.5,
			background: 'black'
		},
		resizable: false,
		title: '',
		width: '800',
		close: function () {
			$(this).dialog('close');
		},
		position: {
			my: 'center',
			at: 'center',
			of: window,
			collision: 'flipfit'
		}
	}
};

module.exports = dialog;

'use strict';

var ajax = require('./ajax'),
	util = require('./util');
/**
 * @function
 * @description Load details to a given gift certificate
 * @param {String} id The ID of the gift certificate
 * @param {Function} callback A function to called
 */
exports.checkBalance = function (id, callback) {
	// load gift certificate details
	var url = util.appendParamToURL(Urls.giftCardCheckBalance, 'giftCertificateID', id);

	ajax.getJson({
		url: url,
		callback: callback
	});
};

'use strict';

var ajax = require('./ajax'),
	minicart = require('./minicart'),
	util = require('./util');

var setAddToCartHandler = function (e) {
	e.preventDefault();
	var form = $(this).closest('form');

	var options = {
		url: util.ajaxUrl(form.attr('action')),
		method: 'POST',
		cache: false,
		data: form.serialize()
	};
	$.ajax(options).done(function (response) {
		if (response.success) {
			ajax.load({
				url: Urls.minicartGC,
				data: {lineItemId: response.result.lineItemId},
				callback: function (response) {
					minicart.show(response);
					form.find('input,textarea').val('');
				}
			});
		} else {
			form.find('span.error').hide();
			for (var id in response.errors.FormErrors) {
				var $errorEl = $('#' + id).addClass('error').removeClass('valid').next('.error');
				if (!$errorEl || $errorEl.length === 0) {
					$errorEl = $('<span for="' + id + '" generated="true" class="error" style=""></span>');
					$('#' + id).after($errorEl);
				}
				$errorEl.text(response.errors.FormErrors[id].replace(/\\'/g, '\'')).show();
			}
		}
	}).fail(function (xhr, textStatus) {
		// failed
		if (textStatus === 'parsererror') {
			window.alert(Resources.BAD_RESPONSE);
		} else {
			window.alert(Resources.SERVER_CONNECTION_ERROR);
		}
	});
};

exports.init = function () {
	$('#AddToBasketButton').on('click', setAddToCartHandler);
};

'use strict';
// jQuery extensions

module.exports = function () {
	// params
	// toggleClass - required
	// triggerSelector - optional. the selector for the element that triggers the event handler. defaults to the child elements of the list.
	// eventName - optional. defaults to 'click'
	$.fn.toggledList = function (options) {
		if (!options.toggleClass) { return this; }
		var list = this;
		return list.on(options.eventName || 'click', options.triggerSelector || list.children(), function (e) {
			e.preventDefault();
			var classTarget = options.triggerSelector ? $(this).parent() : $(this);
			classTarget.toggleClass(options.toggleClass);
			// execute callback if exists
			if (options.callback) {options.callback();}
		});
	};

	$.fn.syncHeight = function () {
		var arr = $.makeArray(this);
		arr.sort(function (a, b) {
			return $(a).height() - $(b).height();
		});
		return this.height($(arr[arr.length - 1]).height());
	};
};

'use strict';

var util = require('./util'),
	bonusProductsView = require('./bonus-products-view');

var timer = {
	id: null,
	clear: function () {
		if (this.id) {
			window.clearTimeout(this.id);
			delete this.id;
		}
	},
	start: function (duration, callback) {
		this.id = setTimeout(callback, duration);
	}
};

var minicart = {
	init: function () {
		this.$el = $('#mini-cart');
		this.$content = this.$el.find('.mini-cart-content');

		$('.mini-cart-product').eq(0).find('.mini-cart-toggle').addClass('fa-caret-down');
		$('.mini-cart-product').not(':first').addClass('collapsed')
			.find('.mini-cart-toggle').addClass('fa-caret-right');

		$('.mini-cart-toggle').on('click', function () {
			$(this).toggleClass('fa-caret-down fa-caret-right');
			$(this).closest('.mini-cart-product').toggleClass('collapsed');
		});

		// events
		this.$el.find('.mini-cart-total').on('mouseenter', function () {
			if (this.$content.not(':visible')) {
				this.slide();
			}
		}.bind(this));

		this.$content.on('mouseenter', function () {
			timer.clear();
		}).on('mouseleave', function () {
			timer.clear();
			timer.start(30, this.close.bind(this));
		}.bind(this));
	},
	/**
	 * @function
	 * @description Shows the given content in the mini cart
	 * @param {String} A HTML string with the content which will be shown
	 */
	show: function (html) {
		this.$el.html(html);
		util.scrollBrowser(0);
		this.init();
		this.slide();
		bonusProductsView.loadBonusOption();
	},
	/**
	 * @function
	 * @description Slides down and show the contents of the mini cart
	 */
	slide: function () {
		timer.clear();
		// show the item
		this.$content.slideDown('slow');
		// after a time out automatically close it
		timer.start(6000, this.close.bind(this));
	},
	/**
	 * @function
	 * @description Closes the mini cart with given delay
	 * @param {Number} delay The delay in milliseconds
	 */
	close: function (delay) {
		timer.clear();
		this.$content.slideUp(delay);
	}
};

module.exports = minicart;

'use strict';

var util = require('./util');

var page = {
	title: '',
	type: '',
	params: util.getQueryStringParams(window.location.search.substr(1)),
	redirect: function (newURL) {
		setTimeout(function () {
			window.location.href = newURL;
		}, 0);
	},
	refresh: function () {
		setTimeout(function () {
			window.location.assign(window.location.href);
		}, 500);
	}
};

module.exports = page;

'use strict';

var giftcert = require('../giftcert'),
	tooltip = require('../tooltip'),
	util = require('../util'),
	dialog = require('../dialog'),
	page = require('../page'),
	validator = require('../validator');

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
	var $form = $('#edit-address-form');

	$form.find('input[name="format"]').remove();
	tooltip.init();
	//$("<input/>").attr({type:"hidden", name:"format", value:"ajax"}).appendTo(form);

	$form.on('click', '.apply-button', function (e) {
		e.preventDefault();
		if (!$form.valid()) {
			return false;
		}
		var url = util.appendParamToURL($form.attr('action'), 'format', 'ajax');
		var applyName = $form.find('.apply-button').attr('name');
		var options = {
			url: url,
			data: $form.serialize() + '&' + applyName + '=x',
			type: 'POST'
		};
		$.ajax(options).done(function (data) {
			if (typeof(data) !== 'string') {
				if (data.success) {
					dialog.close();
					page.refresh();
				} else {
					window.alert(data.message);
					return false;
				}
			} else {
				$('#dialog-container').html(data);
				account.init();
				tooltip.init();
			}
		});
	})
	.on('click', '.cancel-button, .close-button', function (e) {
		e.preventDefault();
		dialog.close();
	})
	.on('click', '.delete-button', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			var url = util.appendParamsToUrl(Urls.deleteAddress, {
				AddressID: $form.find('#addressid').val(),
				format: 'ajax'
			});
			$.ajax({
				url: url,
				method: 'POST',
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					dialog.close();
					page.refresh();
				} else if (data.message.length > 0) {
					window.alert(data.message);
					return false;
				} else {
					dialog.close();
					page.refresh();
				}
			});
		}
	});

	validator.init();
}
/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder () {
	$('.order-items')
		.find('li.hidden:first')
		.prev('li')
		.append('<a class="toggle">View All</a>')
		.children('.toggle')
		.click(function () {
			$(this).parent().siblings('li.hidden').show();
			$(this).remove();
		});
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
	var addresses = $('#addresses');
	if (addresses.length === 0) { return; }

	addresses.on('click', '.address-edit, .address-create', function (e) {
		e.preventDefault();
		dialog.open({
			url: this.href,
			options: {
				open: initializeAddressForm
			}
		});
	}).on('click', '.delete', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			$.ajax({
				url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					page.redirect(Urls.addressesList);
				} else if (data.message.length > 0) {
					window.alert(data.message);
				} else {
					page.refresh();
				}
			});
		}
	});
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {
	$('.add-card').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});

	var paymentList = $('.payment-list');
	if (paymentList.length === 0) { return; }

	util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

	$('form[name="payment-remove"]').on('submit', function (e) {
		e.preventDefault();
		// override form submission in order to prevent refresh issues
		var button = $(this).find('.delete');
		$('<input/>').attr({
			type: 'hidden',
			name: button.attr('name'),
			value: button.attr('value') || 'delete card'
		}).appendTo($(this));
		var data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: data
		})
		.done(function () {
			page.redirect(Urls.paymentsList);
		});
	});
}
/**
 * @private
 * @function
 * @description init events for the loginPage
 */
function initLoginPage() {
	//o-auth binding for which icon is clicked
	$('.oAuthIcon').bind('click', function () {
		$('#OAuthProvider').val(this.id);
	});

	//toggle the value of the rememberme checkbox
	$('#dwfrm_login_rememberme').bind('change', function () {
		if ($('#dwfrm_login_rememberme').attr('checked')) {
			$('#rememberme').val('true');
		} else {
			$('#rememberme').val('false');
		}
	});
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
					$($submit).on('click', function (e) {
						if (!$requestPasswordForm.valid()) {
							return;
						}
						e.preventDefault();
						dialog.submit($submit.attr('name'));
					});
				}
			}
		});
	});
}
/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
	toggleFullOrder();
	initAddressEvents();
	initPaymentEvents();
	initLoginPage();
}

var account = {
	init: function () {
		initializeEvents();
		giftcert.init();
	},
	initCartLogin: function () {
		initLoginPage();
	}
};

module.exports = account;

'use strict';

var account = require('./account'),
	bonusProductsView = require('../bonus-products-view'),
	quickview = require('../quickview'),
	cartStoreInventory = require('../storeinventory/cart');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
	$('#cart-table').on('click', '.item-edit-details a', function (e) {
		e.preventDefault();
		quickview.show({
			url: e.target.href,
			source: 'cart'
		});
	})
	.on('click', '.bonus-item-actions a, .item-details .bonusproducts a', function (e) {
		e.preventDefault();
		bonusProductsView.show(this.href);
	});

	// override enter key for coupon code entry
	$('form input[name$="_couponCode"]').on('keydown', function (e) {
		if (e.which === 13 && $(this).val().length === 0) { return false; }
	});
}

exports.init = function () {
	initializeEvents();
	if (SitePreferences.STORE_PICKUP) {
		cartStoreInventory.init();
	}
	account.initCartLogin();
};

'use strict';

var util = require('../../util');
var shipping = require('./shipping');

/**
 * @function
 * @description Selects the first address from the list of addresses
 */
exports.init = function () {
	var $form = $('.address');
	// select address from list
	$('select[name$="_addressList"]', $form).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $form);
		shipping.updateShippingMethodList();
		// re-validate the form
		$form.validate().form();
	});
};

'use strict';

var ajax = require('../../ajax'),
	formPrepare = require('./formPrepare'),
	giftcard = require('../../giftcard'),
	util = require('../../util');

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
	var $creditCard = $('[data-method="CREDIT_CARD"]');
	$creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
	$creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
	$creditCard.find('input[name*="_creditCard_number"]').val(data.maskedNumber).trigger('change');
	$creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
	$creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
	$creditCard.find('input[name$="_cvn"]').val('').trigger('change');
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
	// load card details
	var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				window.alert(Resources.CC_LOAD_ERROR);
				return false;
			}
			setCCFields(data);
		}
	});
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
	var $paymentMethods = $('.payment-method');
	$paymentMethods.removeClass('payment-method-expanded');

	var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
	if ($selectedPaymentMethod.length === 0) {
		$selectedPaymentMethod = $('[data-method="Custom"]');
	}
	$selectedPaymentMethod.addClass('payment-method-expanded');

	// ensure checkbox of payment method is checked
	$('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
	$('input[value=' + paymentMethodID + ']').prop('checked', 'checked');

	formPrepare.validateForm();
}

/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
	var $checkoutForm = $('.checkout-billing');
	var $addGiftCert = $('#add-giftcert');
	var $giftCertCode = $('input[name$="_giftCertCode"]');
	var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();

	formPrepare.init({
		formSelector: 'form[id$="billing"]',
		continueSelector: '[name$="billing_save"]'
	});

	// default payment method to 'CREDIT_CARD'
	updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : 'CREDIT_CARD');
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		updatePaymentMethod($(this).val());
	});

	// select credit card from list
	$('#creditCardList').on('change', function () {
		var cardUUID = $(this).val();
		if (!cardUUID) {return;}
		populateCreditCardForm(cardUUID);

		// remove server side error
		$('.required.error').removeClass('error');
		$('.error-message').remove();
	});

	$('#check-giftcert').on('click', function (e) {
		e.preventDefault();
		var $balance = $('.balance');
		if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
			var error = $balance.find('span.error');
			if (error.length === 0) {
				error = $('<span>').addClass('error').appendTo($balance);
			}
			error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		giftcard.checkBalance($giftCertCode.val(), function (data) {
			if (!data || !data.giftCertificate) {
				$balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
				return;
			}
			$balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
		});
	});

	$addGiftCert.on('click', function (e) {
		e.preventDefault();
		var code = $giftCertCode.val(),
			$error = $checkoutForm.find('.giftcert-error');
		if (code.length === 0) {
			$error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			} else {
				window.location.assign(Urls.billing);
			}
		});
	});

	$addCoupon.on('click', function (e) {
		e.preventDefault();
		var $error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val();
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			}

			//basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
			//this will force a page refresh to display the coupon message based on a parameter message
			if (data.success && data.baskettotal === 0) {
				window.location.assign(Urls.billing);
			}
		});
	});

	// trigger events on enter
	$couponCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addCoupon.click();
		}
	});
	$giftCertCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addGiftCert.click();
		}
	});
};

'use strict';

var _ = require('lodash');

var $form, $continue, $requiredInputs, validator;

var hasEmptyRequired = function () {
	// filter out only the visible fields
	var requiredValues = $requiredInputs.filter(':visible').map(function () {
		return $(this).val();
	});
	return _(requiredValues).contains('');
};

var validateForm = function () {
	// only validate form when all required fields are filled to avoid
	// throwing errors on empty form
	if (!hasEmptyRequired()) {
		if (validator.form()) {
			$continue.removeAttr('disabled');
		}
	} else {
		$continue.attr('disabled', 'disabled');
	}
};

var validateEl = function () {
	if ($(this).val() === '') {
		$continue.attr('disabled', 'disabled');
	} else {
		// enable continue button on last required field that is valid
		// only validate single field
		if (validator.element(this) && !hasEmptyRequired()) {
			$continue.removeAttr('disabled');
		} else {
			$continue.attr('disabled', 'disabled');
		}
	}
};

var init = function (opts) {
	if (!opts.formSelector || !opts.continueSelector) {
		throw new Error('Missing form and continue action selectors.');
	}
	$form = $(opts.formSelector);
	$continue = $(opts.continueSelector);
	validator = $form.validate();
	$requiredInputs = $('.required', $form).find(':input');
	validateForm();
	// start listening
	$requiredInputs.on('change', validateEl);
	$requiredInputs.filter('input').on('keyup', _.debounce(validateEl, 200));
};

exports.init = init;
exports.validateForm = validateForm;
exports.validateEl = validateEl;

'use strict';

var address = require('./address'),
	billing = require('./billing'),
	multiship = require('./multiship'),
	shipping = require('./shipping');

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
exports.init = function () {
	address.init();
	if ($('.checkout-shipping').length > 0) {
		shipping.init();
	} else if ($('.checkout-multi-shipping').length > 0) {
		multiship.init();
	} else {
		billing.init();
	}

	//if on the order review page and there are products that are not available diable the submit order button
	if ($('.order-summary-footer').length > 0) {
		if ($('.notavailable').length > 0) {
			$('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
		}
	}
};

'use strict';

var address = require('./address'),
	formPrepare = require('./formPrepare'),
	dialog = require('../../dialog'),
	util = require('../../util');

/**
 * @function
 * @description Initializes gift message box for multiship shipping, the message box starts off as hidden and this will display it if the radio button is checked to yes, also added event handler to listen for when a radio button is pressed to display the message box
 */
function initMultiGiftMessageBox() {
	$.each($('.item-list'), function () {
		var $this = $(this);
		var $giftMessage = $this.find('.gift-message-text');

		//handle initial load
		$giftMessage.toggleClass('hidden', $('input[name$="_isGift"]:checked', this).val() !== 'true');

		//set event listeners
		$this.on('change', function () {
			$giftMessage.toggleClass('hidden', $('input[name$="_isGift"]:checked', this).val() !== 'true');
		});
	});
}


/**
 * @function
 * @description capture add edit adddress form events
 */
function addEditAddress(target) {
	var $addressForm = $('form[name$="multishipping_editAddress"]'),
		$addressDropdown = $addressForm.find('select[name$=_addressList]'),
		$addressList = $addressForm.find('.address-list'),
		add = true,
		selectedAddressUUID = $(target).parent().siblings('.select-address').val();

	$addressDropdown.on('change', function (e) {
		e.preventDefault();
		var selectedAddress = $addressList.find('select').val();
		if (selectedAddress !== 'newAddress') {
			selectedAddress = $.grep($addressList.data('addresses'), function (add) {
				return add.UUID === selectedAddress;
			})[0];
			add = false;
			// proceed to fill the form with the selected address
			util.fillAddressFields(selectedAddress, $addressForm);
		} else {
			//reset the form if the value of the option is not a UUID
			$addressForm.find('.input-text, .input-select').val('');
		}
	});

	$addressForm.on('click', '.cancel', function (e) {
		e.preventDefault();
		dialog.close();
	});

	$addressForm.on('submit', function (e) {
		e.preventDefault();
		$.getJSON(Urls.addEditAddress, $addressForm.serialize(), function (response) {
			if (!response.success) {
				// @TODO: figure out a way to handle error on the form
				return;
			}
			var address = response.address,
				$shippingAddress = $(target).closest('.shippingaddress'),
				$select = $shippingAddress.find('.select-address'),
				$selected = $select.find('option:selected'),
				newOption = '<option value="' + address.UUID + '">' +
					((address.ID) ? '(' + address.ID + ')' : address.firstName + ' ' + address.lastName) + ', ' +
					address.address1 + ', ' + address.city + ', ' + address.stateCode + ', ' + address.postalCode +
					'</option>';
			dialog.close();
			if (add) {
				$('.shippingaddress select').removeClass('no-option').append(newOption);
				$('.no-address').hide();
			} else {
				$('.shippingaddress select').find('option[value="' + address.UUID + '"]').html(newOption);
			}
			// if there's no previously selected option, select it
			if ($selected.length === 0 || $selected.val() === '') {
				$select.find('option[value="' + address.UUID + '"]').prop('selected', 'selected').trigger('change');
			}
		});
	});

	//preserve the uuid of the option for the hop up form
	if (selectedAddressUUID) {
		//update the form with selected address
		$addressList.find('option').each(function () {
			//check the values of the options
			if ($(this).attr('value') === selectedAddressUUID) {
				$(this).prop('selected', 'selected');
				$addressDropdown.trigger('change');
			}
		});
	}
}

/**
 * @function
 * @description shows gift message box in multiship, and if the page is the multi shipping address page it will call initmultishipshipaddress() to initialize the form
 */
exports.init = function () {
	initMultiGiftMessageBox();
	if ($('.cart-row .shippingaddress .select-address').length > 0) {
		formPrepare.init({
			continueSelector: '[name$="addressSelection_save"]',
			formSelector: '[id$="multishipping_addressSelection"]'
		});
	}
	$('.edit-address').on('click', 'a', function (e) {
		dialog.open({url: this.href, options: {open: function () {
			address.init();
			addEditAddress(e.target);
		}}});
	});
};

'use strict';

var ajax = require('../../ajax'),
	formPrepare = require('./formPrepare'),
	progress = require('../../progress'),
	tooltip = require('../../tooltip'),
	util = require('../../util');

var shippingMethods;
/**
 * @function
 * @description Initializes gift message box, if shipment is gift
 */
function giftMessageBox() {
	// show gift message box, if shipment is gift
	$('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val() !== 'true');
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
	var $summary = $('#secondary.summary');
	// indicate progress
	progress.show($summary);

	// load the updated summary area
	$summary.load(Urls.summaryRefreshURL, function () {
		// hide edit shipping method link
		$summary.fadeIn('fast');
		$summary.find('.checkout-mini-cart .minishipment .header a').hide();
		$summary.find('.order-totals-table .order-shipping .label a').hide();
	});
}

/**
 * @function
 * @description Helper method which constructs a URL for an AJAX request using the
 * entered address information as URL request parameters.
 */
function getShippingMethodURL(url, extraParams) {
	var $form = $('.address');
	var params = {
		address1: $form.find('input[name$="_address1"]').val(),
		address2: $form.find('input[name$="_address2"]').val(),
		countryCode: $form.find('select[id$="_country"]').val(),
		stateCode: $form.find('select[id$="_state"]').val(),
		postalCode: $form.find('input[name$="_postal"]').val(),
		city: $form.find('input[name$="_city"]').val()
	};
	return util.appendParamsToUrl(url, $.extend(params, extraParams));
}

/**
 * @function
 * @description selects a shipping method for the default shipment and updates the summary section on the right hand side
 * @param
 */
function selectShippingMethod(shippingMethodID) {
	// nothing entered
	if (!shippingMethodID) {
		return;
	}
	// attempt to set shipping method
	var url = getShippingMethodURL(Urls.selectShippingMethodsList, {shippingMethodID: shippingMethodID});
	ajax.getJson({
		url: url,
		callback: function (data) {
			updateSummary();
			if (!data || !data.shippingMethodID) {
				window.alert('Couldn\'t select shipping method.');
				return false;
			}
			// display promotion in UI and update the summary section,
			// if some promotions were applied
			$('.shippingpromotions').empty();


			// if (data.shippingPriceAdjustments && data.shippingPriceAdjustments.length > 0) {
			// 	var len = data.shippingPriceAdjustments.length;
			// 	for (var i=0; i < len; i++) {
			// 		var spa = data.shippingPriceAdjustments[i];
			// 	}
			// }
		}
	});
}

/**
 * @function
 * @description Make an AJAX request to the server to retrieve the list of applicable shipping methods
 * based on the merchandise in the cart and the currently entered shipping address
 * (the address may be only partially entered).  If the list of applicable shipping methods
 * has changed because new address information has been entered, then issue another AJAX
 * request which updates the currently selected shipping method (if needed) and also updates
 * the UI.
 */
function updateShippingMethodList() {
	var $shippingMethodList = $('#shipping-method-list');
	if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
	var url = getShippingMethodURL(Urls.shippingMethodsJSON);

	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				window.alert('Couldn\'t get list of applicable shipping methods.');
				return false;
			}
			if (shippingMethods && shippingMethods.toString() === data.toString()) {
				// No need to update the UI.  The list has not changed.
				return true;
			}

			// We need to update the UI.  The list has changed.
			// Cache the array of returned shipping methods.
			shippingMethods = data;
			// indicate progress
			progress.show($shippingMethodList);

			// load the shipping method form
			var smlUrl = getShippingMethodURL(Urls.shippingMethodsList);
			$shippingMethodList.load(smlUrl, function () {
				$shippingMethodList.fadeIn('fast');
				// rebind the radio buttons onclick function to a handler.
				$shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
					selectShippingMethod($(this).val());
				});

				// update the summary
				updateSummary();
				progress.hide();
				tooltip.init();
				//if nothing is selected in the shipping methods select the first one
				if ($shippingMethodList.find('.input-radio:checked').length === 0) {
					$shippingMethodList.find('.input-radio:first').prop('checked', 'checked');
				}
			});
		}
	});
}

exports.init = function () {
	formPrepare.init({
		continueSelector: '[name$="shippingAddress_save"]',
		formSelector:'[id$="singleshipping_shippingAddress"]'
	});
	$('input[name$="_shippingAddress_isGift"]').on('click', giftMessageBox);

	$('.address').on('change',
		'input[name$="_addressFields_address1"], input[name$="_addressFields_address2"], select[name$="_addressFields_states_state"], input[name$="_addressFields_city"], input[name$="_addressFields_zip"]',
		updateShippingMethodList
	);

	giftMessageBox();
	updateShippingMethodList();
};

exports.updateShippingMethodList = updateShippingMethodList;

'use strict';

var addProductToCart = require('./product/addToCart'),
	ajax = require('../ajax'),
	page = require('../page'),
	productTile = require('../product-tile'),
	quickview = require('../quickview');

/**
 * @private
 * @function
 * @description Binds the click events to the remove-link and quick-view button
 */
function initializeEvents() {
	$('#compare-table').on('click', '.remove-link', function (e) {
		e.preventDefault();
		ajax.getJson({
			url: this.href,
			callback: function () {
				page.refresh();
			}
		});
	})
	.on('click', '.open-quick-view', function (e) {
		e.preventDefault();
		var url = $(this).closest('.product').find('.thumb-link').attr('href');
		quickview.show({
			url: url,
			source: 'quickview'
		});
	});

	$('#compare-category-list').on('change', function () {
		$(this).closest('form').submit();
	});
}

exports.init = function () {
	productTile.init();
	initializeEvents();
	addProductToCart();
};

'use strict';

var dialog = require('../../dialog'),
	minicart = require('../../minicart'),
	page = require('../../page'),
	util = require('../../util'),
	TPromise = require('promise'),
	_ = require('lodash');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]');
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	return TPromise.resolve($.ajax({
		type: 'POST',
		url: util.ajaxUrl(Urls.addProduct),
		data: $form.serialize()
	}));
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function (e) {
	e.preventDefault();
	var $form = $(this).closest('form');

	addItemToCart($form).then(function (response) {
		var $uuid = $form.find('input[name="uuid"]');
		if ($uuid.length > 0 && $uuid.val().length > 0) {
			page.refresh();
		} else {
			// do not close quickview if adding individual item that is part of product set
			// @TODO should notify the user some other way that the add action has completed successfully
			if (!$(this).hasClass('sub-product-item')) {
				dialog.close();
			}
			minicart.show(response);
		}
	}.bind(this));
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
	e.preventDefault();
	var $productForms = $('#product-set-list').find('form').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
		.then(function (responses) {
			dialog.close();
			// show the final response only, which would include all the other items
			minicart.show(responses[responses.length - 1]);
		});
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 * @param {Element} target The target on which an add to cart event-handler will be set
 */
module.exports = function (target) {
	$('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());

	if (target) {
		target.on('click', '.add-to-cart', addToCart);
	} else {
		$('.add-to-cart').on('click', addToCart);
	}

	$('#add-all-to-cart').on('click', addAllToCart);
};

'use strict';

var ajax =  require('../../ajax'),
	util = require('../../util');

var updateContainer = function (data) {
	var $availabilityMsg = $('#pdpMain .availability .availability-msg');
	var message; // this should be lexically scoped, when `let` is supported (ES6)
	if (!data) {
		$availabilityMsg.html(Resources.ITEM_STATUS_NOTAVAILABLE);
		return;
	}
	$availabilityMsg.empty();
	// Look through levels ... if msg is not empty, then create span el
	if (data.levels.IN_STOCK > 0) {
		if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
			// Just in stock
			message = Resources.IN_STOCK;
		} else {
			// In stock with conditions ...
			message = data.inStockMsg;
		}
		$availabilityMsg.append('<p class="in-stock-msg">' + message + '</p>');
	}
	if (data.levels.PREORDER > 0) {
		if (data.levels.IN_STOCK === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
			message = Resources.PREORDER;
		} else {
			message = data.preOrderMsg;
		}
		$availabilityMsg.append('<p class="preorder-msg">' + message + '</p>');
	}
	if (data.levels.BACKORDER > 0) {
		if (data.levels.IN_STOCK === 0 && data.levels.PREORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
			message = Resources.BACKORDER;
		} else {
			message = data.backOrderMsg;
		}
		$availabilityMsg.append('<p class="backorder-msg">' + message + '</p>');
	}
	if (data.inStockDate !== '') {
		$availabilityMsg.append('<p class="in-stock-date-msg">' + String.format(Resources.IN_STOCK_DATE, data.inStockDate) + '</p>');
	}
	if (data.levels.NOT_AVAILABLE > 0) {
		if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.IN_STOCK === 0) {
			message = Resources.NOT_AVAILABLE;
		} else {
			message = Resources.REMAIN_NOT_AVAILABLE;
		}
		$availabilityMsg.append('<p class="not-available-msg">' + message + '</p>');
	}
};

var getAvailability = function () {
	ajax.getJson({
		url: util.appendParamsToUrl(Urls.getAvailability, {
			pid: $('#pid').val(),
			Quantity: $(this).val()
		}),
		callback: updateContainer
	});
};

module.exports = function () {
	$('#pdpMain').on('change', '.pdpForm input[name="Quantity"]', getAvailability);
};

'use strict';
var dialog = require('../../dialog'),
	util = require('../../util');

var zoomMediaQuery = matchMedia('(min-width: 960px)');

/**
 * @description Enables the zoom viewer on the product detail page
 * @param zmq {Media Query List}
 */
var loadZoom = function (zmq) {
	var $imgZoom = $('#pdpMain .main-image'),
		hiresUrl;
	if (!zmq) {
		zmq = zoomMediaQuery;
	}
	if ($imgZoom.length === 0 || dialog.isActive() || util.isMobile() || !zoomMediaQuery.matches) {
		// remove zoom
		$imgZoom.trigger('zoom.destroy');
		return;
	}
	hiresUrl = $imgZoom.attr('href');

	if (hiresUrl && hiresUrl !== 'null' && hiresUrl.indexOf('noimagelarge') === -1 && zoomMediaQuery.matches) {
		$imgZoom.zoom({
			url: hiresUrl
		});
	}
};

zoomMediaQuery.addListener(loadZoom);

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
var setMainImage = function (atts) {
	$('#pdpMain .primary-image').attr({
		src: atts.url,
		alt: atts.alt,
		title: atts.title
	});
	if (!dialog.isActive() && !util.isMobile()) {
		$('#pdpMain .main-image').attr('href', atts.hires);
	}
	loadZoom();
};

/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
var replaceImages = function () {
	var $newImages = $('#update-images'),
		$imageContainer = $('#pdpMain .product-image-container');
	if ($newImages.length === 0) { return; }

	$imageContainer.html($newImages.html());
	$newImages.remove();
	loadZoom();
};

/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
	if (dialog.isActive() || util.isMobile()) {
		$('#pdpMain .main-image').removeAttr('href');
	}
	loadZoom();
	// handle product thumbnail click event
	$('#pdpMain').on('click', '.productthumbnail', function () {
		// switch indicator
		$(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
		$(this).closest('.thumb').addClass('selected');

		setMainImage($(this).data('lgimg'));
	});
};
module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;

'use strict';

var dialog = require('../../dialog'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	addToCart = require('./addToCart'),
	availability = require('./availability'),
	image = require('./image'),
	productNav = require('./productNav'),
	productSet = require('./productSet'),
	recommendations = require('./recommendations'),
	variant = require('./variant');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
	$('#pdpMain .product-detail .product-tabs').tabs();
	productNav();
	recommendations();
	tooltip.init();
}

/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
	var $pdpMain = $('#pdpMain');

	addToCart();
	availability();
	variant();
	image();
	productSet();
	if (SitePreferences.STORE_PICKUP) {
		productStoreInventory.init();
	}

	// Add to Wishlist and Add to Gift Registry links behaviors
	$pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
		var data = util.getQueryStringParams($('.pdpForm').serialize());
		if (data.cartAction) {
			delete data.cartAction;
		}
		var url = util.appendParamsToUrl(this.href, data);
		this.setAttribute('href', url);
	});

	// product options
	$pdpMain.on('change', '.product-options select', function () {
		var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
		var selectedItem = $(this).children().filter(':selected').first();
		salesPrice.text(selectedItem.data('combined'));
	});

	// prevent default behavior of thumbnail link and add this Button
	$pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
		e.preventDefault();
	});

	$('.size-chart-link a').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
}

var product = {
	initializeEvents: initializeEvents,
	init: function () {
		initializeDom();
		initializeEvents();
	}
};

module.exports = product;

'use strict';

var ajax = require('../../ajax'),
	util = require('../../util');

/**
 * @description loads product's navigation
 **/
module.exports = function () {
	var $pidInput = $('.pdpForm input[name="pid"]').last(),
		$navContainer = $('#product-nav-container');
	// if no hash exists, or no pid exists, or nav container does not exist, return
	if (window.location.hash.length <= 1 || $pidInput.length === 0 || $navContainer.length === 0) {
		return;
	}

	var pid = $pidInput.val(),
		hash = window.location.hash.substr(1),
		url = util.appendParamToURL(Urls.productNav + '?' + hash, 'pid', pid);

	ajax.load({
		url: url,
		target: $navContainer
	});
};

'use strict';

var addToCart = require('./addToCart'),
	ajax = require('../../ajax'),
	tooltip = require('../../tooltip'),
	util = require('../../util');

module.exports = function () {
	var $addToCart = $('#add-to-cart'),
		$addAllToCart = $('#add-all-to-cart'),
		$productSetList = $('#product-set-list');

	var updateAddToCartButtons = function () {
		if ($productSetList.find('.add-to-cart[disabled]').length > 0) {
			$addAllToCart.attr('disabled', 'disabled');
			// product set does not have an add-to-cart button, but product bundle does
			$addToCart.attr('disabled', 'disabled');
		} else {
			$addAllToCart.removeAttr('disabled');
			$addToCart.removeAttr('disabled');
		}
	};

	if ($productSetList.length > 0) {
		updateAddToCartButtons();
	}
	// click on swatch for product set
	$productSetList.on('click', '.product-set-item .swatchanchor', function (e) {
		e.preventDefault();
		if ($(this).parents('li').hasClass('unselectable')) { return; }
		var url = Urls.getSetItem + this.search;
		var $container = $(this).closest('.product-set-item');
		var qty = $container.find('form input[name="Quantity"]').first().val();

		ajax.load({
			url: util.appendParamToURL(url, 'Quantity', isNaN(qty) ? '1' : qty),
			target: $container,
			callback: function () {
				updateAddToCartButtons();
				addToCart($container);
				tooltip.init();
			}
		});
	});
};

'use strict';

/**
 * @description Creates product recommendation carousel using jQuery jcarousel plugin
 **/
module.exports = function () {
	var $carousel = $('#carousel-recommendations');
	if (!$carousel || $carousel.length === 0 || $carousel.children().length === 0) {
		return;
	}
	$carousel.jcarousel();
	$('#carousel-recommendations .jcarousel-prev')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '-=1'
		});

	$('#carousel-recommendations .jcarousel-next')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '+=1'
		});
};

'use strict';

var addToCart = require('./addToCart'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util');


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href) {
	var $pdpForm = $('.pdpForm');
	var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	};

	progress.show($('#pdpMain'));

	ajax.load({
		url: util.appendParamsToUrl(href, params),
		target: $('#product-content'),
		callback: function () {
			addToCart();
			if (SitePreferences.STORE_PICKUP) {
				productStoreInventory.init();
			}
			image.replaceImages();
			tooltip.init();
		}
	});
};

module.exports = function () {
	var $pdpMain = $('#pdpMain');
	// hover on swatch - should update main image with swatch image
	$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});

	// click on swatch - should replace product content with new variant
	$pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
		e.preventDefault();
		if ($(this).parents('li').hasClass('unselectable')) { return; }
		updateContent(this.href);
	});

	// change drop down variation attribute - should replace product content with new variant
	$pdpMain.on('change', '.variation-select', function () {
		if ($(this).val().length === 0) { return; }
		updateContent($(this).val());
	});
};

'use strict';

var addProductToCart = require('./product/addToCart'),
	ajax = require('../ajax'),
	quickview = require('../quickview'),
	util = require('../util');

/**
 * @function
 * @description Loads address details to a given address and fills the address form
 * @param {String} addressID The ID of the address to which data will be loaded
 */
function populateForm(addressID, $form) {
	// load address details
	var url = Urls.giftRegAdd + addressID;
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data || !data.address) {
				window.alert(Resources.REG_ADDR_ERROR);
				return false;
			}
			// fill the form
			$form.find('[name$="_addressid"]').val(data.address.ID);
			$form.find('[name$="_firstname"]').val(data.address.firstName);
			$form.find('[name$="_lastname"]').val(data.address.lastName);
			$form.find('[name$="_address1"]').val(data.address.address1);
			$form.find('[name$="_address2"]').val(data.address.address2);
			$form.find('[name$="_city"]').val(data.address.city);
			$form.find('[name$="_country"]').val(data.address.countryCode).trigger('change');
			$form.find('[name$="_postal"]').val(data.address.postalCode);
			$form.find('[name$="_state"]').val(data.address.stateCode);
			$form.find('[name$="_phone"]').val(data.address.phone);
			// $form.parent('form').validate().form();
		}
	});
}

/**
 * @private
 * @function
 * @description Initializes events for the gift registration
 */
function initializeEvents() {
	var $eventAddressForm = $('form[name$="_giftregistry"]'),
		$beforeAddress = $eventAddressForm.find('fieldset[name="address-before"]'),
		$afterAddress = $eventAddressForm.find('fieldset[name="address-after"]');

	$('.usepreevent').on('click', function () {
		// filter out storefront toolkit
		$(':input', $beforeAddress).not('[id^="ext"]').not('select[name$="_addressBeforeList"]').each(function () {
			var fieldName = $(this).attr('name'),
				$afterField = $afterAddress.find('[name="' + fieldName.replace('Before', 'After') + '"]');
			$afterField.val($(this).val()).trigger('change');
		});
	});
	$eventAddressForm.on('change', 'select[name$="_addressBeforeList"]', function () {
		var addressID = $(this).val();
		if (addressID.length === 0) { return; }
		populateForm(addressID, $beforeAddress);
	})
	.on('change', 'select[name$="_addressAfterList"]', function () {
		var addressID = $(this).val();
		if (addressID.length === 0) { return; }
		populateForm(addressID, $afterAddress);
	});

	$('form[name$="_giftregistry_items"]').on('click', '.item-details a', function (e) {
		e.preventDefault();
		var productListID = $('input[name=productListID]').val();
		quickview.show({
			url: e.target.href,
			source: 'giftregistry',
			productlistid: productListID
		});
	});
}

exports.init = function () {
	initializeEvents();
	addProductToCart();

	util.setDeleteConfirmation('.item-list', String.format(Resources.CONFIRM_DELETE, Resources.TITLE_GIFTREGISTRY));
};

'use strict';

var compareWidget = require('../compare-widget'),
	productTile = require('../product-tile'),
	progress = require('../progress'),
	util = require('../util');

function infiniteScroll() {
	// getting the hidden div, which is the placeholder for the next page
	var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
	// get url hidden in DOM
	var gridUrl = loadingPlaceHolder.attr('data-grid-url');

	if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
		// switch state to 'loading'
		// - switches state, so the above selector is only matching once
		// - shows loading indicator
		loadingPlaceHolder.attr('data-loading-state', 'loading');
		loadingPlaceHolder.addClass('infinite-scroll-loading');

		/**
		 * named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
		 */
		var fillEndlessScrollChunk = function (html) {
			loadingPlaceHolder.removeClass('infinite-scroll-loading');
			loadingPlaceHolder.attr('data-loading-state', 'loaded');
			$('div.search-result-content').append(html);
		};

		// old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
		// it was removed to temporarily address RAP-2649
		if (false) {
			// if we hit the cache
			fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
		} else {
			// else do query via ajax
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: gridUrl,
				success: function (response) {
					// put response into cache
					try {
						sessionStorage['scroll-cache_' + gridUrl] = response;
					} catch (e) {
						// nothing to catch in case of out of memory of session storage
						// it will fall back to load via ajax
					}
					// update UI
					fillEndlessScrollChunk(response);
					productTile.init();
				}
			});
		}
	}
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing() {
	var hash = location.href.split('#')[1];
	if (hash === 'results-content' || hash === 'results-products') { return; }
	var refineUrl;

	if (hash.length > 0) {
		refineUrl = window.location.pathname + '?' + hash;
	} else {
		return;
	}
	progress.show($('.search-result-content'));
	$('#main').load(util.appendParamToURL(refineUrl, 'format', 'ajax'), function () {
		compareWidget.init();
		productTile.init();
		progress.hide();
	});
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $('#main');
	// compare checked
	$main.on('click', 'input[type="checkbox"].compare-check', function () {
		var cb = $(this);
		var tile = cb.closest('.product-tile');

		var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
		var itemImg = tile.find('.product-image a img').first();
		func({
			itemid: tile.data('itemid'),
			uuid: tile[0].id,
			img: itemImg,
			cb: cb
		});

	});

	// handle toggle refinement blocks
	$main.on('click', '.refinement h3', function () {
		$(this).toggleClass('expanded')
		.siblings('ul').toggle();
	});

	// handle events for updating grid
	$main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a', function () {
		if ($(this).parent().hasClass('unselectable')) { return; }
		var catparent = $(this).parents('.category-refinement');
		var folderparent = $(this).parents('.folder-refinement');

		//if the anchor tag is uunderneath a div with the class names & , prevent the double encoding of the url
		//else handle the encoding for the url
		if (catparent.length > 0 || folderparent.length > 0) {
			return true;
		} else {
			var query = util.getQueryString(this.href);
			if (query.length > 1) {
				window.location.hash = query;
			} else {
				window.location.href = this.href;
			}
			return false;
		}
	});

	// handle events item click. append params.
	$main.on('click', '.product-tile a:not("#quickviewbutton")', function () {
		var a = $(this);
		// get current page refinement values
		var wl = window.location;

		var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
		var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

		// merge hash params with querystring params
		var params = $.extend(hashParams, qsParams);
		if (!params.start) {
			params.start = 0;
		}
		// get the index of the selected item and save as start parameter
		var tile = a.closest('.product-tile');
		var idx = tile.data('idx') ? + tile.data('idx') : 0;

		// convert params.start to integer and add index
		params.start = (+params.start) + (idx + 1);
		// set the hash and allow normal action to continue
		a[0].hash = $.param(params);
	});

	// handle sorting change
	$main.on('change', '.sort-by select', function () {
		var refineUrl = $(this).find('option:selected').val();
		var queryString = util.getQueryString(refineUrl);
		window.location.hash = queryString;
		return false;
	})
	.on('change', '.items-per-page select', function () {
		var refineUrl = $(this).find('option:selected').val();
		var queryString = util.getQueryString(refineUrl);
		if (refineUrl === 'INFINITE_SCROLL') {
			$('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
		} else {
			$('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
			window.location.hash = queryString;
		}
		return false;
	});

	// handle hash change
	window.onhashchange = updateProductListing;
}

exports.init = function () {
	compareWidget.init();
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$(window).on('scroll', infiniteScroll);
	}
	productTile.init();
	initializeEvents();
};

'use strict';
exports.init = function () {
	$('#homepage-slider')
		// responsive slides
		.on('jcarousel:create jcarousel:reload', function () {
			var element = $(this),
				width = element.innerWidth();
			element.jcarousel('items').css('width', width + 'px');
		})
		.jcarousel({
			wrap: 'circular'
		})
		.jcarouselAutoscroll({
			interval: 5000
		});
	$('#homepage-slider .jcarousel-control')
		.on('jcarouselpagination:active', 'a', function () {
			$(this).addClass('active');
		})
		.on('jcarouselpagination:inactive', 'a', function () {
			$(this).removeClass('active');
		})
		.jcarouselPagination({
			item: function (page) {
				return '<a href="#' + page + '">' + page + '</a>';
			}
		});

	$('#vertical-carousel')
		.jcarousel({
			vertical: true
		})
		.jcarouselAutoscroll({
			interval: 5000
		});
	$('#vertical-carousel .jcarousel-prev')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '-=1'
		});

	$('#vertical-carousel .jcarousel-next')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '+=1'
		});
};

'use strict';
var dialog = require('../dialog');

exports.init = function () {
	$('.store-details-link').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
};

'use strict';

var addProductToCart = require('./product/addToCart'),
	page = require('../page'),
	util = require('../util');

exports.init = function () {
	addProductToCart();
	$('#editAddress').on('change', function () {
		page.redirect(util.appendParamToURL(Urls.wishlistAddress, 'AddressID', $(this).val()));
	});

	//add js logic to remove the , from the qty feild to pass regex expression on client side
	$('.option-quantity-desired input').on('focusout', function () {
		$(this).val($(this).val().replace(',', ''));
	});
};

'use strict';

var imagesLoaded = require('imagesloaded'),
	quickview = require('./quickview');

function initQuickViewButtons() {
	$('.tiles-container .product-image').on('mouseenter', function () {
		var $qvButton = $('#quickviewbutton');
		if ($qvButton.length === 0) {
			$qvButton = $('<a id="quickviewbutton" class="quickview">' + Resources.QUICK_VIEW + '<i class="fa fa-arrows-alt"></i></a>');
		}
		var $link = $(this).find('.thumb-link');
		$qvButton.attr({
			'href': $link.attr('href'),
			'title': $link.attr('title')
		}).appendTo(this);
		$qvButton.on('click', function (e) {
			e.preventDefault();
			quickview.show({
				url: $(this).attr('href'),
				source: 'quickview'
			});
		});
	});
}

function gridViewToggle() {
	$('.toggle-grid').on('click', function () {
		$('.search-result-content').toggleClass('wide-tiles');
		$(this).toggleClass('wide');
	});
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	initQuickViewButtons();
	gridViewToggle();
	$('.swatch-list').on('mouseleave', function () {
		// Restore current thumb image
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $thumb.data('current');

		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	$('.swatch-list .swatch').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) { return; }

		var $tile = $(this).closest('.product-tile');
		$(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
		$(this).addClass('selected');
		$tile.find('.thumb-link').attr('href', $(this).attr('href'));
		$tile.find('name-link').attr('href', $(this).attr('href'));

		var data = $(this).children('img').filter(':first').data('thumb');
		var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
		var currentAttrs = {
			src: data.src,
			alt: data.alt,
			title: data.title
		};
		$thumb.attr(currentAttrs);
		$thumb.data('current', currentAttrs);
	}).on('mouseenter', function () {
		// get current thumb details
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $(this).children('img').filter(':first').data('thumb'),
			current = $thumb.data('current');

		// If this is the first time, then record the current img
		if (!current) {
			$thumb.data('current', {
				src: $thumb[0].src,
				alt: $thumb[0].alt,
				title: $thumb[0].title
			});
		}

		// Set the tile image to the values provided on the swatch data attributes
		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
}

exports.init = function () {
	var $tiles = $('.tiles-container .product-tile');
	if ($tiles.length === 0) { return; }
	imagesLoaded('.tiles-container').on('done', function () {
		$tiles.syncHeight()
			.each(function (idx) {
				$(this).data('idx', idx);
			});
	});
	initializeEvents();
};

'use strict';

var $loader;

/**
 * @function
 * @description Shows an AJAX-loader on top of a given container
 * @param {Element} container The Element on top of which the AJAX-Loader will be shown
 */
var show = function (container) {
	var target = (!container || $(container).length === 0) ? $('body') : $(container);
	$loader = $loader || $('.loader');

	if ($loader.length === 0) {
		$loader = $('<div/>').addClass('loader')
			.append($('<div/>').addClass('loader-indicator'), $('<div/>').addClass('loader-bg'));
	}
	return $loader.appendTo(target).show();
};
/**
 * @function
 * @description Hides an AJAX-loader
 */
var hide = function () {
	if ($loader) {
		$loader.hide();
	}
};

exports.show = show;
exports.hide = hide;

'use strict';

var dialog = require('./dialog'),
	product = require('./pages/product'),
	util = require('./util'),
	_ = require('lodash');


var makeUrl = function (url, source, productListID) {
	if (source) {
		url = util.appendParamToURL(url, 'source', source);
	}
	if (productListID) {
		url = util.appendParamToURL(url, 'productlistid', productListID);
	}
	return url;
};

var removeParam = function (url) {
	if (url.indexOf('?') !== -1) {
		return url.substring(0, url.indexOf('?'));
	} else {
		return url;
	}
};

var quickview = {
	init: function () {
		if (!this.exists()) {
			this.$container = $('<div/>').attr('id', 'QuickViewDialog').appendTo(document.body);
		}
		this.productLinks = $('#search-result-items .thumb-link').map(function (index, thumbLink) {
			return $(thumbLink).attr('href');
		});
	},

	setup: function (qvUrl) {
		var $btnNext = $('.quickview-next'),
			$btnPrev = $('.quickview-prev');

		product.initializeEvents();

		this.productLinkIndex = _(this.productLinks).findIndex(function (url) {
			return removeParam(url) === removeParam(qvUrl);
		});

		// hide the buttons on the compare page or when there are no other products
		if (this.productLinks.length <= 1 || $('.compareremovecell').length > 0) {
			$btnNext.hide();
			$btnPrev.hide();
			return;
		}

		if (this.productLinkIndex === this.productLinks.length - 1) {
			$btnNext.attr('disabled', 'disabled');
		}
		if (this.productLinkIndex === 0) {
			$btnPrev.attr('disabled', 'disabled');
		}

		$btnNext.on('click', function (e) {
			e.preventDefault();
			this.navigateQuickview(1);
		}.bind(this));
		$btnPrev.on('click', function (e) {
			e.preventDefault();
			this.navigateQuickview(-1);
		}.bind(this));
	},

	/**
	 * @param {Number} step - How many products away from current product to navigate to. Negative number means navigate backward
	 */
	navigateQuickview: function (step) {
		// default step to 0
		this.productLinkIndex += (step ? step : 0);
		var url = makeUrl(this.productLinks[this.productLinkIndex], 'quickview');
		dialog.replace({
			url: url,
			callback: this.setup.bind(this, url)
		});
	},

	/**
	 * @description show quick view dialog
	 * @param {Object} options
	 * @param {String} options.url - url of the product details
	 * @param {String} options.source - source of the dialog to be appended to URL
	 * @param {String} options.productlistid - to be appended to URL
	 * @param {Function} options.callback - callback once the dialog is opened
	 */
	show: function (options) {
		var url;
		if (!this.exists()) {
			this.init();
		}
		url = makeUrl(options.url, options.source, options.productlistid);

		dialog.open({
			target: this.$container,
			url: url,
			options: {
				width: 920,
				title: Resources.QUICK_VIEW_POPUP,
				open: function () {
					this.setup(url);
					if (typeof options.callback === 'function') { options.callback(); }
				}.bind(this)
			}
		});
	},
	exists: function () {
		return this.$container && (this.$container.length > 0);
	}
};

module.exports = quickview;

'use strict';

/**
 * copied from https://github.com/darkskyapp/string-hash
 */
function hashFn(str) {
	var hash = 5381,
		i = str.length;

	while (i) {
		hash = (hash * 33) ^ str.charCodeAt(--i);
	}
	/* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
	* integers. Since we want the results to be always positive, convert the
	* signed int to an unsigned by doing an unsigned bitshift. */
	return hash >>> 0;
}

/**
 * Create rating based on hash ranging from 2-5
 * @param pid
 */
function getRating(pid) {
	return hashFn(pid.toString()) % 30 / 10 + 2;
}

module.exports = {
	init: function () {
		$('.product-review').each(function (index, review) {
			var pid = $(review).data('pid');
			if (!pid) {
				return;
			}
			// rating range from 2 - 5
			var rating = getRating(pid);
			var baseRating = Math.floor(rating);
			var starsCount = 0;
			for (var i = 0; i < baseRating; i++) {
				$('.rating', review).append('<i class="fa fa-star"></i>');
				starsCount++;
			}
			// give half star for anything in between
			if (rating > baseRating) {
				$('.rating', review).append('<i class="fa fa-star-half-o"></i>');
				starsCount++;
			}
			if (starsCount < 5) {
				for (var j = 0; j < 5 - starsCount; j++) {
					$('.rating', review).append('<i class="fa fa-star-o"></i>');
				}
			}
		});
	}
};

'use strict';

/**
 * @private
 * @function
 * @description Binds event to the place holder (.blur)
 */
function initializeEvents() {
	$('#q').focus(function () {
		var input = $(this);
		if (input.val() === input.attr('placeholder')) {
			input.val('');
		}
	})
	.blur(function () {
		var input = $(this);
		if (input.val() === '' || input.val() === input.attr('placeholder')) {
			input.val(input.attr('placeholder'));
		}
	})
	.blur();
}

exports.init = initializeEvents;

'use strict';

var util = require('./util');

var currentQuery = null,
	lastQuery = null,
	runningQuery = null,
	listTotal = -1,
	listCurrent = -1,
	delay = 30,
	$resultsContainer;
/**
 * @function
 * @description Handles keyboard's arrow keys
 * @param keyCode Code of an arrow key to be handled
 */
function handleArrowKeys(keyCode) {
	switch (keyCode) {
		case 38:
			// keyUp
			listCurrent = (listCurrent <= 0) ? (listTotal - 1) : (listCurrent - 1);
			break;
		case 40:
			// keyDown
			listCurrent = (listCurrent >= listTotal - 1) ? 0 : listCurrent + 1;
			break;
		default:
			// reset
			listCurrent = -1;
			return false;
	}

	$resultsContainer.children().removeClass('selected').eq(listCurrent).addClass('selected');
	$('input[name="q"]').val($resultsContainer.find('.selected .suggestionterm').first().text());
	return true;
}

var searchsuggest = {
	/**
	 * @function
	 * @description Configures parameters and required object instances
	 */
	init: function (container, defaultValue) {
		var $searchContainer = $(container);
		var $searchForm = $searchContainer.find('form[name="simpleSearch"]');
		var $searchField = $searchForm.find('input[name="q"]');

		// disable browser auto complete
		$searchField.attr('autocomplete', 'off');

		// on focus listener (clear default value)
		$searchField.focus(function () {
			if (!$resultsContainer) {
				// create results container if needed
				$resultsContainer = $('<div/>').attr('id', 'search-suggestions').appendTo($searchContainer);
			}
			if ($searchField.val() === defaultValue) {
				$searchField.val('');
			}
		});
		// on blur listener
		$(document).on('click', function (e) {
			if (!$searchContainer.is(e.target)) {
				setTimeout(this.clearResults, 200);
			}
		}.bind(this));
		// on key up listener
		$searchField.keyup(function (e) {

			// get keyCode (window.event is for IE)
			var keyCode = e.keyCode || window.event.keyCode;

			// check and treat up and down arrows
			if (handleArrowKeys(keyCode)) {
				return;
			}
			// check for an ENTER or ESC
			if (keyCode === 13 || keyCode === 27) {
				this.clearResults();
				return;
			}

			currentQuery = $searchField.val().trim();

			// no query currently running, init a update
			if (runningQuery === null) {
				runningQuery = currentQuery;
				setTimeout(this.suggest.bind(this), delay);
			}
		}.bind(this));
	},

	/**
	 * @function
	 * @description trigger suggest action
	 */
	suggest: function () {
		// check whether query to execute (runningQuery) is still up to date and had not changed in the meanwhile
		// (we had a little delay)
		if (runningQuery !== currentQuery) {
			// update running query to the most recent search phrase
			runningQuery = currentQuery;
		}

		// if it's empty clear the results box and return
		if (runningQuery.length === 0) {
			this.clearResults();
			runningQuery = null;
			return;
		}

		// if the current search phrase is the same as for the last suggestion call, just return
		if (lastQuery === runningQuery) {
			runningQuery = null;
			return;
		}

		// build the request url
		var reqUrl = util.appendParamToURL(Urls.searchsuggest, 'q', runningQuery);
		reqUrl = util.appendParamToURL(reqUrl, 'legacy', 'false');

		// execute server call
		$.get(reqUrl, function (data) {
			var suggestionHTML = data,
				ansLength = suggestionHTML.trim().length;

			// if there are results populate the results div
			if (ansLength === 0) {
				this.clearResults();
			} else {
				// update the results div
				$resultsContainer.html(suggestionHTML).fadeIn(200);
			}

			// record the query that has been executed
			lastQuery = runningQuery;
			// reset currently running query
			runningQuery = null;

			// check for another required update (if current search phrase is different from just executed call)
			if (currentQuery !== lastQuery) {
				// ... and execute immediately if search has changed while this server call was in transit
				runningQuery = currentQuery;
				setTimeout(this.suggest.bind(this), delay);
			}
			this.hideLeftPanel();
		}.bind(this));
	},
	/**
	 * @function
	 * @description
	 */
	clearResults: function () {
		if (!$resultsContainer) { return; }
		$resultsContainer.fadeOut(200, function () {$resultsContainer.empty();});
	},
	/**
	 * @function
	 * @description
	 */
	hideLeftPanel: function () {
		//hide left panel if there is only a matching suggested custom phrase
		if ($('.search-suggestion-left-panel-hit').length === 1 && $('.search-phrase-suggestion a').text().replace(/(^[\s]+|[\s]+$)/g, '').toUpperCase() === $('.search-suggestion-left-panel-hit a').text().toUpperCase()) {
			$('.search-suggestion-left-panel').css('display', 'none');
			$('.search-suggestion-wrapper-full').addClass('search-suggestion-wrapper');
			$('.search-suggestion-wrapper').removeClass('search-suggestion-wrapper-full');
		}
	}
};

module.exports = searchsuggest;

'use strict';

var util = require('./util');

var qlen = 0,
	listTotal = -1,
	listCurrent = -1,
	delay = 300,
	fieldDefault = null,
	suggestionsJson = null,
	$searchForm,
	$searchField,
	$searchContainer,
	$resultsContainer;
/**
 * @function
 * @description Handles keyboard's arrow keys
 * @param keyCode Code of an arrow key to be handled
 */
function handleArrowKeys(keyCode) {
	switch (keyCode) {
		case 38:
			// keyUp
			listCurrent = (listCurrent <= 0) ? (listTotal - 1) : (listCurrent - 1);
			break;
		case 40:
			// keyDown
			listCurrent = (listCurrent >= listTotal - 1) ? 0 : listCurrent + 1;
			break;
		default:
			// reset
			listCurrent = -1;
			return false;
	}

	$resultsContainer.children().removeClass('selected').eq(listCurrent).addClass('selected');
	$searchField.val($resultsContainer.find('.selected .suggestionterm').first().text());
	return true;
}
var searchsuggest = {
	/**
	 * @function
	 * @description Configures parameters and required object instances
	 */
	init: function (container, defaultValue) {
		// initialize vars
		$searchContainer = $(container);
		$searchForm = $searchContainer.find('form[name="simpleSearch"]');
		$searchField = $searchForm.find('input[name="q"]');
		fieldDefault = defaultValue;

		// disable browser auto complete
		$searchField.attr('autocomplete', 'off');

		// on focus listener (clear default value)
		$searchField.focus(function () {
			if (!$resultsContainer) {
				// create results container if needed
				$resultsContainer = $('<div/>').attr('id', 'suggestions').appendTo($searchContainer).css({
					'top': $searchContainer[0].offsetHeight,
					'left': 0,
					'width': $searchField[0].offsetWidth
				});
			}
			if ($searchField.val() === fieldDefault) {
				$searchField.val('');
			}
		});
		// on blur listener
		$searchField.blur(function () {
			setTimeout(this.clearResults, 200);
		}.bind(this));
		// on key up listener
		$searchField.keyup(function (e) {

			// get keyCode (window.event is for IE)
			var keyCode = e.keyCode || window.event.keyCode;

			// check and treat up and down arrows
			if (handleArrowKeys(keyCode)) {
				return;
			}
			// check for an ENTER or ESC
			if (keyCode === 13 || keyCode === 27) {
				this.clearResults();
				return;
			}

			var lastVal = $searchField.val();

			// if is text, call with delay
			setTimeout(function () {
				this.suggest(lastVal);
			}.bind(this), delay);
		}.bind(this));
		// on submit we do not submit the form, but change the window location
		// in order to avoid https to http warnings in the browser
		// only if it's not the default value and it's not empty
		$searchForm.submit(function (e) {
			e.preventDefault();
			var searchTerm = $searchField.val();
			if (searchTerm === fieldDefault || searchTerm.length === 0) {
				return false;
			}
			window.location = util.appendParamToURL($(this).attr('action'), 'q', searchTerm);
		});
	},

	/**
	 * @function
	 * @description trigger suggest action
	 * @param lastValue
	 */
	suggest: function (lastValue) {
		// get the field value
		var part = $searchField.val();

		// if it's empty clear the resuts box and return
		if (part.length === 0) {
			this.clearResults();
			return;
		}

		// if part is not equal to the value from the initiated call,
		// or there were no results in the last call and the query length
		// is longer than the last query length, return
		// #TODO: improve this to look at the query value and length
		if ((lastValue !== part) || (listTotal === 0 && part.length > qlen)) {
			return;
		}
		qlen = part.length;

		// build the request url
		var reqUrl = util.appendParamToURL(Urls.searchsuggest, 'q', part);
		reqUrl = util.appendParamToURL(reqUrl, 'legacy', 'true');

		// get remote data as JSON
		$.getJSON(reqUrl, function (data) {
			// get the total of results
			var suggestions = data,
				ansLength = suggestions.length;

			// if there are results populate the results div
			if (ansLength === 0) {
				this.clearResults();
				return;
			}
			suggestionsJson = suggestions;
			var html = '';
			for (var i = 0; i < ansLength; i++) {
				html += '<div><div class="suggestionterm">' + suggestions[i].suggestion + '</div><span class="hits">' + suggestions[i].hits + '</span></div>';
			}

			// update the results div
			$resultsContainer.html(html).show().on('hover', 'div', function () {
				$(this).toggleClass = 'selected';
			}).on('click', 'div', function () {
				// on click copy suggestion to search field, hide the list and submit the search
				$searchField.val($(this).children('.suggestionterm').text());
				this.clearResults();
				$searchForm.trigger('submit');
			}.bind(this));
		}.bind(this));
	},
	/**
	 * @function
	 * @description
	 */
	clearResults: function () {
		if (!$resultsContainer) { return; }
		$resultsContainer.empty().hide();
	}
};

module.exports = searchsuggest;

'use strict';

var inventory = require('./');

var cartInventory = {
	setSelectedStore: function (storeId) {
		var $selectedStore = $('.store-tile.' + storeId),
			$lineItem = $('.cart-row[data-uuid="' + this.uuid + '"]'),
			storeAddress = $selectedStore.find('.store-address').html(),
			storeStatus = $selectedStore.find('.store-status').data('status'),
			storeStatusText = $selectedStore.find('.store-status').text();
		this.selectedStore = storeId;

		$lineItem.find('.instore-delivery .selected-store-address')
			.data('storeId', storeId)
			.attr('data-store-id', storeId)
			.html(storeAddress);
		$lineItem.find('.instore-delivery .selected-store-availability')
			.data('status', storeStatus)
			.attr('data-status', storeStatus)
			.text(storeStatusText);
		$lineItem.find('.instore-delivery .delivery-option').removeAttr('disabled').trigger('click');
	},
	cartSelectStore: function (selectedStore) {
		var self = this;
		inventory.getStoresInventory(this.uuid).then(function (stores) {
			inventory.selectStoreDialog({
				stores: stores,
				selectedStoreId: selectedStore,
				selectedStoreText: Resources.SELECTED_STORE,
				continueCallback: function () {},
				selectStoreCallback: self.setSelectedStore.bind(self)
			});
		}).done();
	},
	setDeliveryOption: function (value, storeId) {
		// set loading state
		$('.item-delivery-options')
			.addClass('loading')
			.children().hide();

		var data = {
			plid: this.uuid,
			storepickup: (value === 'store' ? true : false)
		};
		if (value === 'store') {
			data.storepickup = true;
			data.storeid = storeId;
		} else {
			data.storepickup = false;
		}
		$.ajax({
			url: Urls.setStorePickup,
			data: data,
			success: function () {
				// remove loading state
				$('.item-delivery-options')
					.removeClass('loading')
					.children().show();
			}
		});
	},
	init: function () {
		var self = this;
		$('.item-delivery-options .set-preferred-store').on('click', function (e) {
			e.preventDefault();
			self.uuid = $(this).data('uuid');
			var selectedStore = $(this).closest('.instore-delivery').find('.selected-store-address').data('storeId');
			if (!User.zip) {
				inventory.zipPrompt(function () {
					self.cartSelectStore(selectedStore);
				});
			} else {
				self.cartSelectStore(selectedStore);
			}
		});
		$('.item-delivery-options .delivery-option').on('click', function () {
			// reset the uuid
			var selectedStore = $(this).closest('.instore-delivery').find('.selected-store-address').data('storeId');
			self.uuid = $(this).closest('.cart-row').data('uuid');
			self.setDeliveryOption($(this).val(), selectedStore);
		});
	}
};

module.exports = cartInventory;

'use strict';

var _ = require('lodash'),
	dialog = require('../dialog'),
	TPromise = require('promise'),
	util = require('../util');

var newLine = '\n';
var storeTemplate = function (store, selectedStoreId, selectedStoreText) {
	return [
		'<li class="store-tile ' + store.storeId + (store.storeId === selectedStoreId ? ' selected' : '') + '">',
		'	<p class="store-address">',
		'		' + store.address1 + '<br/>',
		'		' + store.city + ', ' + store.stateCode + ' ' + store.postalCode,
		'	</p>',
		'	<p class="store-status" data-status="' + store.statusclass + '">' + store.status + '</p>',
		'	<button class="select-store-button" data-store-id="' + store.storeId + '"' +
		(store.statusclass !== 'store-in-stock' ? 'disabled="disabled"' : '') + '>',
		'		' + (store.storeId === selectedStoreId ? selectedStoreText : Resources.SELECT_STORE),
		'	</button>',
		'</li>'
	].join(newLine);
};

var storeListTemplate = function (stores, selectedStoreId, selectedStoreText) {
	if (stores && stores.length) {
		return [
			'<div class="store-list-container">',
			'<ul class="store-list">',
			_.map(stores, function (store) {
				return storeTemplate(store, selectedStoreId, selectedStoreText);
			}).join(newLine),
			'</ul>',
			'</div>',
			'<div class="store-list-pagination">',
			'</div>'
		].join(newLine);
	} else {
		return '<div class="no-results">' + Resources.INVALID_ZIP + '</div>';
	}
};

var zipPromptTemplate = function () {
	return [
		'<div id="preferred-store-panel">',
		'	<input type="text" id="user-zip" placeholder="' + Resources.ENTER_ZIP + '" name="zipCode"/>',
		'</div>'
	].join(newLine);
};

/**
 * @description test whether zipcode is valid for either US or Canada
 * @return {Boolean} true if the zipcode is valid for either country, false if it's invalid for both
 **/
var validateZipCode = function (zipCode) {
	var regexes = {
		canada: /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i,
		usa: /^\d{5}(-\d{4})?$/
	},
		valid = false;
	if (!zipCode) { return; }
	_.each(regexes, function (re) {
		var regexp = new RegExp(re);
		valid = regexp.test(zipCode);
	});
	return valid;
};

var storeinventory = {
	zipPrompt: function (callback) {
		var self = this;
		dialog.open({
			html: zipPromptTemplate(),
			options: {
				title: Resources.STORE_NEAR_YOU,
				width: 500,
				buttons: [{
					text: Resources.SEARCH,
					click: function () {
						var zipCode = $('#user-zip').val();
						if (validateZipCode(zipCode)) {
							self.setUserZip(zipCode);
							if (callback) {
								callback(zipCode);
							}
						}
					}
				}],
				open: function () {
					$('#user-zip').on('keypress', function (e) {
						if (e.which === 13) {
							// trigger the search button
							$('.ui-dialog-buttonset .ui-button').trigger('click');
						}
					});
				}
			}
		});
	},
	getStoresInventory: function (pid) {
		return TPromise.resolve($.ajax({
			url: util.appendParamsToUrl(Urls.storesInventory, {
				pid: pid,
				zipCode: User.zip
			}),
			dataType: 'json'
		}));
	},
	/**
	 * @description open the dialog to select store
	 * @param {Array} options.stores
	 * @param {String} options.selectedStoreId
	 * @param {String} options.selectedStoreText
	 * @param {Function} options.continueCallback
	 * @param {Function} options.selectStoreCallback
	 **/
	selectStoreDialog: function (options) {
		var self = this,
			stores = options.stores,
			selectedStoreId = options.selectedStoreId,
			selectedStoreText = options.selectedStoreText,
			storeList = storeListTemplate(stores, selectedStoreId, selectedStoreText);
		dialog.open({
			html: storeList,
			options: {
				title: Resources.SELECT_STORE + ' - ' + User.zip,
				buttons: [{
						text: Resources.CHANGE_LOCATION,
					click: function () {
						self.setUserZip(null);
						// trigger the event to start the process all over again
						$('.set-preferred-store').trigger('click');
					}.bind(this)
				}, {
					text: Resources.CONTINUE,
					click: function () {
						if (options.continueCallback) {
							options.continueCallback(stores);
						}
						dialog.close();
					}
				}],
				open: function () {
					$('.select-store-button').on('click', function (e) {
						e.preventDefault();
						var storeId = $(this).data('storeId');
						// if the store is already selected, don't select again
						if (storeId === selectedStoreId) { return; }
						$('.store-list .store-tile.selected').removeClass('selected')
							.find('.select-store-button').text(Resources.SELECT_STORE);
						$(this).text(selectedStoreText)
							.closest('.store-tile').addClass('selected');
						if (options.selectStoreCallback) {
							options.selectStoreCallback(storeId);
						}
					});
				}
			}
		});
	},
	setUserZip: function (zip) {
		User.zip = zip;
		$.ajax({
			type: 'POST',
			url: Urls.setZipCode,
			data: {
				zipCode: zip
			}
		});
	},
	shippingLoad: function () {
		var $checkoutForm = $('.address');
		$checkoutForm.off('click');
		$checkoutForm.on('click', 'input[name$="_shippingAddress_isGift"]', function () {
			$(this).parent().siblings('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val());
		});
	}
};

module.exports = storeinventory;

'use strict';

var _ = require('lodash'),
	inventory = require('./');

var newLine = '\n';
var pdpStoreTemplate = function (store) {
	return [
		'<li class="store-list-item ' + (store.storeId === User.storeId ? ' selected' : '') + '">',
		'	<div class="store-address">' + store.address1 + ', ' + store.city + ' ' + store.stateCode +
		' ' + store.postalCode + '</div>',
		'	<div class="store-status" data-status="' + store.statusclass + '">' + store.status + '</div>',
		'</li>'
	].join(newLine);
};
var pdpStoresListingTemplate = function (stores) {
	if (stores && stores.length) {
		return [
			'<div class="store-list-pdp-container">',
			(stores.length > 1 ? '	<a class="stores-toggle collapsed" href="#">' + Resources.SEE_MORE + '</a>' : ''),
			'	<ul class="store-list-pdp">',
			_.map(stores, pdpStoreTemplate).join(newLine),
			'	</ul>',
			'</div>'
		].join(newLine);
	}
};

var storesListing = function (stores) {
	// list all stores on PDP page
	if ($('.store-list-pdp-container').length) {
		$('.store-list-pdp-container').remove();
	}
	$('.availability-results').append(pdpStoresListingTemplate(stores));
};

var productInventory = {
	setPreferredStore: function (storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStore,
			type: 'POST',
			data: {storeId: storeId}
		});
	},
	productSelectStore: function () {
		var self = this;
		inventory.getStoresInventory(this.pid).then(function (stores) {
			inventory.selectStoreDialog({
				stores: stores,
				selectedStoreId: User.storeId,
				selectedStoreText: Resources.PREFERRED_STORE,
				continueCallback: storesListing,
				selectStoreCallback: self.setPreferredStore
			});
		}).done();
	},
	init: function () {
		var $availabilityContainer = $('.availability-results'),
			self = this;
		this.pid = $('input[name="pid"]').val();

		$('#product-content .set-preferred-store').on('click', function (e) {
			e.preventDefault();
			if (!User.zip) {
				inventory.zipPrompt(function () {
					self.productSelectStore();
				});
			} else {
				self.productSelectStore();
			}
		});

		if ($availabilityContainer.length) {
			if (User.storeId) {
				inventory.getStoresInventory(this.pid).then(storesListing);
			}

			// See more or less stores in the listing
			$availabilityContainer.on('click', '.stores-toggle', function (e) {
				e.preventDefault();
				$('.store-list-pdp .store-list-item').toggleClass('visible');
				if ($(this).hasClass('collapsed')) {
					$(this).text(Resources.SEE_LESS);
				} else {
					$(this).text(Resources.SEE_MORE);
				}
				$(this).toggleClass('collapsed');
			});
		}
	}
};

module.exports = productInventory;

'use strict';

/**
 * @function
 * @description Initializes the tooltip-content and layout
 */
exports.init = function () {
	$(document).tooltip({
		items: '.tooltip',
		track: true,
		content: function () {
			return $(this).find('.tooltip-content').html();
		}
	});

	$('.share-link').on('click', function (e) {
		e.preventDefault();
		var target = $(this).data('target');
		if (!target) {
			return;
		}
		$(target).toggleClass('active');
	});
};

'use strict';

var _ = require('lodash');

var util = {
	/**
	 * @function
	 * @description appends the parameter with the given name and value to the given url and returns the changed url
	 * @param {String} url the url to which the parameter will be added
	 * @param {String} name the name of the parameter
	 * @param {String} value the value of the parameter
	 */
	appendParamToURL: function (url, name, value) {
		// quit if the param already exists
		if (url.indexOf(name + '=') !== -1) {
			return url;
		}
		var separator = url.indexOf('?') !== -1 ? '&' : '?';
		return url + separator + name + '=' + encodeURIComponent(value);
	},
	/**
	 * @function
	 * @description appends the parameters to the given url and returns the changed url
	 * @param {String} url the url to which the parameters will be added
	 * @param {Object} params
	 */
	appendParamsToUrl: function (url, params) {
		var _url = url;
		_.each(params, function (value, name) {
			_url = this.appendParamToURL(_url, name, value);
		}.bind(this));
		return _url;
	},
	/**
	 * @function
	 * @description extract the query string from URL
	 * @param {String} url the url to extra query string from
	 **/
	getQueryString: function (url) {
		var qs;
		if (!_.isString(url)) { return; }
		var a = document.createElement('a');
		a.href = url;
		if (a.search) {
			qs = a.search.substr(1); // remove the leading ?
		}
		return qs;
	},
	/**
	 * @function
	 * @description
	 * @param {String}
	 * @param {String}
	 */
	elementInViewport: function (el, offsetToTop) {
		var top = el.offsetTop,
			left = el.offsetLeft,
			width = el.offsetWidth,
			height = el.offsetHeight;

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
			left += el.offsetLeft;
		}

		if (typeof(offsetToTop) !== 'undefined') {
			top -= offsetToTop;
		}

		if (window.pageXOffset !== null) {
			return (
				top < (window.pageYOffset + window.innerHeight) &&
				left < (window.pageXOffset + window.innerWidth) &&
				(top + height) > window.pageYOffset &&
				(left + width) > window.pageXOffset
			);
		}

		if (document.compatMode === 'CSS1Compat') {
			return (
				top < (window.document.documentElement.scrollTop + window.document.documentElement.clientHeight) &&
				left < (window.document.documentElement.scrollLeft + window.document.documentElement.clientWidth) &&
				(top + height) > window.document.documentElement.scrollTop &&
				(left + width) > window.document.documentElement.scrollLeft
			);
		}
	},

	/**
	 * @function
	 * @description Appends the parameter 'format=ajax' to a given path
	 * @param {String} path the relative path
	 */
	ajaxUrl: function (path) {
		return this.appendParamToURL(path, 'format', 'ajax');
	},

	/**
	 * @function
	 * @description
	 * @param {String} url
	 */
	toAbsoluteUrl: function (url) {
		if (url.indexOf('http') !== 0 && url.charAt(0) !== '/') {
			url = '/' + url;
		}
		return url;
	},
	/**
	 * @function
	 * @description Loads css dynamically from given urls
	 * @param {Array} urls Array of urls from which css will be dynamically loaded.
	 */
	loadDynamicCss: function (urls) {
		var i, len = urls.length;
		for (i = 0; i < len; i++) {
			this.loadedCssFiles.push(this.loadCssFile(urls[i]));
		}
	},

	/**
	 * @function
	 * @description Loads css file dynamically from given url
	 * @param {String} url The url from which css file will be dynamically loaded.
	 */
	loadCssFile: function (url) {
		return $('<link/>').appendTo($('head')).attr({
			type: 'text/css',
			rel: 'stylesheet'
		}).attr('href', url); // for i.e. <9, href must be added after link has been appended to head
	},
	// array to keep track of the dynamically loaded CSS files
	loadedCssFiles: [],

	/**
	 * @function
	 * @description Removes all css files which were dynamically loaded
	 */
	clearDynamicCss: function () {
		var i = this.loadedCssFiles.length;
		while (0 > i--) {
			$(this.loadedCssFiles[i]).remove();
		}
		this.loadedCssFiles = [];
	},
	/**
	 * @function
	 * @description Extracts all parameters from a given query string into an object
	 * @param {String} qs The query string from which the parameters will be extracted
	 */
	getQueryStringParams: function (qs) {
		if (!qs || qs.length === 0) { return {}; }
		var params = {},
			unescapedQS = decodeURIComponent(qs);
		// Use the String::replace method to iterate over each
		// name-value pair in the string.
		unescapedQS.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
			function ($0, $1, $2, $3) {
				params[$1] = $3;
			}
		);
		return params;
	},

	fillAddressFields: function (address, $form) {
		for (var field in address) {
			if (field === 'ID' || field === 'UUID' || field === 'key') {
				continue;
			}
			// if the key in address object ends with 'Code', remove that suffix
			// keys that ends with 'Code' are postalCode, stateCode and countryCode
			$form.find('[name$="' + field.replace('Code', '') + '"]').val(address[field]);
			// update the state fields
			if (field === 'countryCode') {
				$form.find('[name$="country"]').trigger('change');
				// retrigger state selection after country has changed
				// this results in duplication of the state code, but is a necessary evil
				// for now because sometimes countryCode comes after stateCode
				$form.find('[name$="state"]').val(address.stateCode);
			}
		}
	},
	/**
	 * @function
	 * @description Updates the number of the remaining character
	 * based on the character limit in a text area
	 */
	limitCharacters: function () {
		$('form').find('textarea[data-character-limit]').each(function () {
			var characterLimit = $(this).data('character-limit');
			var charCountHtml = String.format(Resources.CHAR_LIMIT_MSG,
				'<span class="char-remain-count">' + characterLimit + '</span>',
				'<span class="char-allowed-count">' + characterLimit + '</span>');
			var charCountContainer = $(this).next('div.char-count');
			if (charCountContainer.length === 0) {
				charCountContainer = $('<div class="char-count"/>').insertAfter($(this));
			}
			charCountContainer.html(charCountHtml);
			// trigger the keydown event so that any existing character data is calculated
			$(this).change();
		});
	},
	/**
	 * @function
	 * @description Binds the onclick-event to a delete button on a given container,
	 * which opens a confirmation box with a given message
	 * @param {String} container The name of element to which the function will be bind
	 * @param {String} message The message the will be shown upon a click
	 */
	setDeleteConfirmation: function (container, message) {
		$(container).on('click', '.delete', function () {
			return window.confirm(message);
		});
	},
	/**
	 * @function
	 * @description Scrolls a browser window to a given x point
	 * @param {String} The x coordinate
	 */
	scrollBrowser: function (xLocation) {
		$('html, body').animate({scrollTop: xLocation}, 500);
	},

	isMobile: function () {
		var mobileAgentHash = ['mobile', 'tablet', 'phone', 'ipad', 'ipod', 'android', 'blackberry', 'windows ce', 'opera mini', 'palm'];
		var	idx = 0;
		var isMobile = false;
		var userAgent = (navigator.userAgent).toLowerCase();

		while (mobileAgentHash[idx] && !isMobile) {
			isMobile = (userAgent.indexOf(mobileAgentHash[idx]) >= 0);
			idx++;
		}
		return isMobile;
	}
};

module.exports = util;

'use strict';

var naPhone = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
var regex = {
	phone: {
		us: naPhone,
		ca: naPhone,
		fr: /^0[1-6]{1}(([0-9]{2}){4})|((\s[0-9]{2}){4})|((-[0-9]{2}){4})$/,
		it: /^(([0-9]{2,4})([-\s\/]{0,1})([0-9]{4,8}))?$/,
		jp: /^(0\d{1,4}- ?)?\d{1,4}-\d{4}$/,
		cn: /.*/,
		gb: /^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/
	},
	postal: {
		us: /^\d{5}(-\d{4})?$/,
		ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
		fr: /^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$/,
		it: /^([0-9]){5}$/,
		jp: /^([0-9]){3}[-]([0-9]){4}$/,
		cn: /^([0-9]){6}$/,
		gb: /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/
	},
	notCC: /^(?!(([0-9 -]){13,19})).*$/
};
// global form validator settings
var settings = {
	errorClass: 'error',
	errorElement: 'span',
	onkeyup: false,
	onfocusout: function (element) {
		if (!this.checkable(element)) {
			this.element(element);
		}
	}
};
/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value, el) {
	var country = $(el).closest('form').find('.country');
	if (country.length === 0 || country.val().length === 0 || !regex.phone[country.val().toLowerCase()]) {
		return true;
	}

	var rgx = regex.phone[country.val().toLowerCase()];
	var isOptional = this.optional(el);
	var isValid = rgx.test($.trim(value));

	return isOptional || isValid;
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
	var isValid = regex.notCC.test($.trim(value));
	return isValid;
};

/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
	var isOptional = this.optional(el);
	var isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
	return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
	if ($.trim(value).length === 0) { return true; }
	return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

$.extend($.validator.messages, {
	required: Resources.VALIDATE_REQUIRED,
	remote: Resources.VALIDATE_REMOTE,
	email: Resources.VALIDATE_EMAIL,
	url: Resources.VALIDATE_URL,
	date: Resources.VALIDATE_DATE,
	dateISO: Resources.VALIDATE_DATEISO,
	number: Resources.VALIDATE_NUMBER,
	digits: Resources.VALIDATE_DIGITS,
	creditcard: Resources.VALIDATE_CREDITCARD,
	equalTo: Resources.VALIDATE_EQUALTO,
	maxlength: $.validator.format(Resources.VALIDATE_MAXLENGTH),
	minlength: $.validator.format(Resources.VALIDATE_MINLENGTH),
	rangelength: $.validator.format(Resources.VALIDATE_RANGELENGTH),
	range: $.validator.format(Resources.VALIDATE_RANGE),
	max: $.validator.format(Resources.VALIDATE_MAX),
	min: $.validator.format(Resources.VALIDATE_MIN)
});

var validator = {
	regex: regex,
	settings: settings,
	init: function () {
		var self = this;
		$('form:not(.suppress)').each(function () {
			$(this).validate(self.settings);
		});
	},
	initForm: function (f) {
		$(f).validate(this.settings);
	}
};

module.exports = validator;
